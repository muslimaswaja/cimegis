//~ Credit: https://halistechnology.com/2015/05/28/use-javascript-to-export-your-data-as-csv/

function convertArrayOfObjectsToCSV(args) {
	var result, ctr, keys, columnDelimiter, lineDelimiter, data;

	data = args.data || null;
	
	if (data == null || !data.length) {
		return null;
	}

	columnDelimiter = args.columnDelimiter || ',';
	lineDelimiter = args.lineDelimiter || '\n';
	keys = Object.keys(data[0]);
	columnTitle = camelToSentence(keys);
	result = '';
	result += columnTitle.join(columnDelimiter);
	result += lineDelimiter;

	data.forEach(function(item) {
		ctr = 0;
		
		keys.forEach(function(key) {
			if (ctr > 0) result += columnDelimiter;

			result += item[key];
			ctr++;
		});
		
		result += lineDelimiter;
	});

	return result;
}
    
function downloadCSV() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			prosesDownloadCSV(JSON.parse(this.responseText));
		}
	};

	xhttp.open("GET", "/api/csv-laporan-kriminalitas", true);
	xhttp.send();
}

function prosesDownloadCSV(dataJSON) {
	var data, filename, link;
	
	var csv = convertArrayOfObjectsToCSV({
		data: dataJSON
	});
	
	if (csv == null) return;
	
	let d = new Date();
	filename = 'Laporan ' + d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + "-" + d.getHours() + d.getMinutes() + d.getSeconds() + '.csv';
	var blob = new Blob([csv], {type: "text/csv;charset=utf-8;"});

	if (navigator.msSaveBlob) { // IE 10+
		navigator.msSaveBlob(blob, filename)
	} else {
		var link = document.createElement("a");
		
		if (link.download !== undefined) {
			// feature detection, Browsers that support HTML5 download attribute
			var url = URL.createObjectURL(blob);
			
			link.setAttribute("href", url);
			link.setAttribute("download", filename);
			
			link.style = "visibility:hidden";
			
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}
}

function camelToSentence(dataArray) {
	let sentenceText = [];
	
	dataArray.forEach(function(datum) {
		if(datum == "waktuKejadian") {
			sentenceText.push("Waktu");
			sentenceText.push("Tanggal");
		} else {
			//~ Credit: https://stackoverflow.com/questions/7225407/convert-camelcasetext-to-sentence-case-text
			let whitespacedText = datum.replace( /([A-Z])/g, " $1" );
			let uppercasedText = whitespacedText.charAt(0).toUpperCase() + whitespacedText.slice(1);
			
			sentenceText.push(uppercasedText);
		}
	});
	
	return sentenceText;
}
