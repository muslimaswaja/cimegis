package com.muslimaswaja;

import com.muslimaswaja.object.*;
import com.muslimaswaja.repository.*;
import com.muslimaswaja.api.*;
import com.muslimaswaja.tools.*;
import com.muslimaswaja.controller.*;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;
import org.jooby.apitool.ApiTool;
import org.jooby.handlers.CorsHandler;
import org.jooby.hbs.Hbs;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

/**
 * @author jooby generator
 */

public class App extends Jooby {
	{
		use("*", new CorsHandler());
		use(new Jackson());
		use(new Jdbc());
		use(new Hbs());

		use(new Jdbi3()
			/** Install SqlObjectPlugin */
			.doWith(jdbi -> {
				jdbi.installPlugin(new SqlObjectPlugin());
			})
			/** Creates a transaction per request */
			.transactionPerRequest(
				new TransactionalRequest()
					.attach(FeaturesR.class)
					.attach(JenisLokasiR.class)
					.attach(JenisKriminalitasR.class)
					.attach(UserAccountR.class)
					.attach(KonstantaR.class)
					.attach(KoefisienR.class)
					.attach(LaporanKriminalR.class)
					.attach(LaporanKriminalWargaR.class)
					.attach(FeaturesDashboardR.class)
					.attach(ErrorR.class)
					.attach(WilayahKerjaR.class)
			)
		);
		
		use(new ApiTool()
			.swagger("/swagger")
		);
		
		//~ Asset script
		assets("/assets/**");
		
		//~ Halaman
		assets("/tambah-laporan", "add-crime.html");
		assets("/view-laporan", "view-crime.html");
    
		//~ APIs
		use(new FeaturesA());
		use(new JenisLokasiA());
		use(new JenisKriminalitasA());
		use(new UserAccountA());
		use(new UserAccountA());
		use(new KoefisienA());
		use(new LaporanKriminalA());
		use(new LaporanKriminalWargaA());
		use(new MetodeA());
		use(new WilayahKerjaA());
		
		//~ Routes
		use(new Routes());
		
		//~ Controller
		use(new SudoC());
		use(new AdminC());
	}
	
	public static void main(final String[] args) {
		run(App::new, args);
	}

}
