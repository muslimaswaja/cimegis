package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LaporanKriminalWargaO {

  public static class Mapper implements RowMapper<LaporanKriminalWargaO> {
    @Override public LaporanKriminalWargaO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new LaporanKriminalWargaO(rs.getInt("id"), rs.getInt("id_jenis_kriminalitas"), rs.getString("waktu_kejadian"), rs.getString("lokasi"));
    }
  }

  private int id;
  private int idJenisKriminalitas;
  private String lokasi;
  private String waktuKejadian;

  public LaporanKriminalWargaO(int id, int id_jenis_kriminalitas, String waktu_kejadian, String lokasi) {
    this.id = id;
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.lokasi = lokasi;
    this.waktuKejadian = waktu_kejadian;
  }

  public int getId() {
    return id;
  }

  public int getIdJenisKriminalitas() {
    return idJenisKriminalitas;
  }

  public String getLokasi() {
    return lokasi;
  }

  public String getWaktuKejadian() {
    return waktuKejadian;
  }
}
