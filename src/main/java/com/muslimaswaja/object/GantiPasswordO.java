package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GantiPasswordO {

  public static class Mapper implements RowMapper<GantiPasswordO> {
    @Override public GantiPasswordO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new GantiPasswordO(rs.getInt("id"), rs.getString("password"));
    }
  }

  private int id;
  private String password;

  public GantiPasswordO(int id, String password) {
    this.id = id;
    this.password = password;
  }
  
  public int getId() {
	  return id;
  }
  
  public String getPassword() {
	  return password;
  }
  
  public void setPassword(String password) {
	  this.password = password;
  }
}
