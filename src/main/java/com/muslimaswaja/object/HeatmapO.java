package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HeatmapO {

  public static class Mapper implements RowMapper<HeatmapO> {
    @Override public HeatmapO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new HeatmapO(rs.getString("lokasi"));
    }
  }
  
  private String lokasi;
  private double bobot;

  public HeatmapO(String lokasi) {
	  this.lokasi = lokasi;
	  this.bobot = 1;
  }

  public String getLokasi() {
    return lokasi;
  }

  public double getBobot() {
    return bobot;
  }

  public void setBobot(double bobot) {
    this.bobot = bobot;
  }
}
