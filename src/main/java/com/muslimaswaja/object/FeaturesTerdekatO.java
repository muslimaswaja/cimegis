package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.lang.Math;

public class FeaturesTerdekatO implements Comparable<FeaturesTerdekatO> {

  public static class Mapper implements RowMapper<FeaturesTerdekatO> {
    @Override public FeaturesTerdekatO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new FeaturesTerdekatO(rs.getString("nama"), rs.getString("jenis_lokasi"), rs.getString("lokasi"), rs.getDouble("jarak"));
    }
  }

  private String nama;
  private String jenisLokasi;
  private int jarak;
  private double jarakAsli;
  private String lokasi;

  public FeaturesTerdekatO(String nama, String jenis_lokasi, String lokasi, double jarak) {
    this.nama = nama;
    this.jenisLokasi = jenis_lokasi;
    this.lokasi = lokasi;
    this.jarak = (int) Math.round(jarak);
    this.jarakAsli = jarak;
  }

  public String getNama() {
    return nama;
  }

  public String getJenisLokasi() {
    return jenisLokasi;
  }

  public String getLokasi() {
    return lokasi;
  }

  public int getJarak() {
    return jarak;
  }

  public double getJarakAsli() {
    return jarakAsli;
  }
  
  @Override
  public int compareTo(FeaturesTerdekatO o) {
	Integer jarakOInteger = (Integer) o.getJarak();
	Integer jarakInteger = (Integer) jarak;
    return jarakInteger.compareTo(jarakOInteger);
  }
}
