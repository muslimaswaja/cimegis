package com.muslimaswaja.object;

import java.util.List;
import java.util.ArrayList;

public class JarakFeaturesO {

  private int id;
  private List<Double> jarak = new ArrayList<>();

  public JarakFeaturesO(int id, List<Double> jarak) {
    this.id = id;
    this.jarak = jarak;
  }
  
  public int getId() {
	  return id;
  }
  
  public List<Double> getJarak() {
	  return jarak;
  }
}
