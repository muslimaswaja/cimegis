package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.List;
import java.util.ArrayList;

public class KonstantaO {
	
  public static class Mapper implements RowMapper<KonstantaO> {
    @Override public KonstantaO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new KonstantaO(rs.getInt("id"), rs.getInt("id_jenis_kriminalitas"), rs.getString("nilai"), rs.getString("waktu_input"));
    }
  }

  private int id;
  private int idJenisKriminalitas;
  private String nilai;
  private String waktuInput;

  public KonstantaO(int id, int id_jenis_kriminalitas, String nilai, String waktu_input) {
    this.id = id;
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.nilai = nilai;
    this.waktuInput = waktu_input;
  }
  
  public int getId() {
	  return id;
  }
  
  public int getIdJenisKriminalitas() {
	  return idJenisKriminalitas;
  }
  
  public String getNilai() {
	  return nilai;
  }
  
  public String getWaktuInput() {
	  return waktuInput;
  }
}
