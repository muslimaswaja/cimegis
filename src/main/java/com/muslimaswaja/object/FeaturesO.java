package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeaturesO {

  public static class Mapper implements RowMapper<FeaturesO> {
    @Override public FeaturesO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new FeaturesO(rs.getInt("id"), rs.getString("nama"), rs.getInt("id_jenis_lokasi"), rs.getString("st_astext"), rs.getString("waktu_input"), rs.getString("keterangan"));
    }
  }

  private int id;
  private String nama;
  private int idJenisLokasi;
  private String lokasi;
  private String waktuInput;
  private String keterangan;

  public FeaturesO(int id, String nama, int id_jenis_lokasi, String st_astext, String waktu_input, String keterangan) {
    this.id = id;
    this.nama = nama;
    this.idJenisLokasi = id_jenis_lokasi;
    this.lokasi = st_astext;
    this.waktuInput = waktu_input;
    this.keterangan = keterangan;
  }

  public int getId() {
    return id;
  }

  public String getNama() {
    return nama;
  }

  public int getIdJenisLokasi() {
    return idJenisLokasi;
  }

  public String getLokasi() {
    return lokasi;
  }

  public String getWaktuInput() {
    return waktuInput;
  }

  public String getKeterangan() {
    return keterangan;
  }

  public void setWaktuInput(String waktuInput) {
    this.waktuInput = waktuInput;
  }
}
