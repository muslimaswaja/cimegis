package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeaturesDashboardO {

  public static class Mapper implements RowMapper<FeaturesDashboardO> {
    @Override public FeaturesDashboardO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new FeaturesDashboardO(rs.getString("jenis"), rs.getInt("jumlah"));
    }
  }

  private String jenis;
  private int jumlah;

  public FeaturesDashboardO(String jenis, int jumlah) {
    this.jenis = jenis;
    this.jumlah = jumlah;
  }
  
  public String getJenis() {
	  return jenis;
  }
  
  public int getJumlah() {
	  return jumlah;
  }
}
