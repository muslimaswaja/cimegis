package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserAccountO {

  public static class Mapper implements RowMapper<UserAccountO> {
    @Override public UserAccountO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new UserAccountO(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("nama"), rs.getInt("role"));
    }
  }

  private int id;
  private String username;
  private String password;
  private String nama;
  private int role;

  public UserAccountO(int id, String username, String password, String nama, int role) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.nama = nama;
    this.role = role;
  }
  
  public int getId() {
	  return id;
  }
  
  public String getUsername() {
	  return username;
  }
  
  public String getPassword() {
	  return password;
  }
  
  public String getNama() {
	  return nama;
  }
  
  public int getRole() {
	  return role;
  }
  
  public void setPassword(String password) {
	  this.password = password;
  }
}
