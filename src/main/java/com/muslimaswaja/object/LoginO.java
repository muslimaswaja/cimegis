package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginO {

  public static class Mapper implements RowMapper<LoginO> {
    @Override public LoginO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new LoginO(rs.getString("username"), rs.getString("password"));
    }
  }

  private String username;
  private String password;

  public LoginO(String username, String password) {
    this.username = username;
    this.password = password;
  }
  
  public String getUsername() {
	  return username;
  }
  
  public String getPassword() {
	  return password;
  }
}
