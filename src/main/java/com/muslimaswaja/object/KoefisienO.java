package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class KoefisienO {

  public static class Mapper implements RowMapper<KoefisienO> {
    @Override public KoefisienO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new KoefisienO(rs.getInt("id"), rs.getInt("id_jenis_kriminalitas"), rs.getString("nilai"), rs.getString("waktu_input"), rs.getInt("id_jenis_lokasi"), rs.getDouble("korelasi"));
    }
  }

  private int id;
  private int idJenisKriminalitas;
  private int idJenisLokasi;
  private double korelasi;
  private String nilai;
  private String waktuInput;

  public KoefisienO(int id, int id_jenis_kriminalitas, String nilai, String waktu_input, int id_jenis_lokasi, double korelasi) {
    this.id = id;
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.idJenisLokasi = id_jenis_lokasi;
    this.korelasi = korelasi;
    this.nilai = nilai;
    this.waktuInput = waktu_input;
  }
  
  public int getId() {
	  return id;
  }
  
  public int getIdJenisKriminalitas() {
	  return idJenisKriminalitas;
  }
  
  public int getIdJenisLokasi() {
	  return idJenisLokasi;
  }
  
  public String getNilai() {
	  return nilai;
  }
  
  public String getWaktuInput() {
	  return waktuInput;
  }
  
  public double getKorelasi() {
	  return korelasi;
  }
  
  public void setWaktuInput(String waktuInput) {
    this.waktuInput = waktuInput;
  }
}
