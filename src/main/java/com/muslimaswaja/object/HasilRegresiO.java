package com.muslimaswaja.object;

import java.util.List;
import java.util.ArrayList;

public class HasilRegresiO implements Comparable<HasilRegresiO> {

  private int idJenisKriminalitas;
  private double nilai;

  public HasilRegresiO(int idJenisKriminalitas, double nilai) {
    this.idJenisKriminalitas = idJenisKriminalitas;
    this.nilai = nilai;
  }
  
  public int getIdJenisKriminalitas() {
	  return idJenisKriminalitas;
  }
  
  public double getNilai() {
	  return nilai;
  }
  
  @Override
  public int compareTo(HasilRegresiO o) {
	Double hasilRegresiO = (Double) o.getNilai();
	Double hasilRegresi = (Double) nilai;
    return hasilRegresiO.compareTo(hasilRegresi);
  }
}
