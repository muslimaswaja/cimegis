package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JumlahKriminalitasO {

  public static class Mapper implements RowMapper<JumlahKriminalitasO> {
    @Override public JumlahKriminalitasO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new JumlahKriminalitasO(rs.getInt("id_jenis_kriminalitas"), rs.getInt("bulan"), rs.getInt("jumlah"), rs.getDouble("error"));
    }
  }

  private int idJenisKriminalitas;
  private int bulan;
  private int jumlah;
  private double error;

  public JumlahKriminalitasO(int id_jenis_kriminalitas, int bulan, int jumlah, double error) {
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.bulan = bulan;
    this.jumlah = jumlah;
    this.error = error;
  }
  
  public int getIdJenisKriminalitas() {
	  return idJenisKriminalitas;
  }
  
  public int getBulan() {
	  return bulan;
  }
  
  public int getJumlah() {
	  return jumlah;
  }
  
  public double getError() {
	  return error;
  }
  
  public void setJumlah(int jumlah) {
	  this.jumlah = jumlah;
  }
  
  public void setError(double error) {
	  this.error = error;
  }
}
