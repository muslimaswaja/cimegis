package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LaporanKriminalO {

  public static class Mapper implements RowMapper<LaporanKriminalO> {
    @Override public LaporanKriminalO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new LaporanKriminalO(rs.getInt("id"), rs.getInt("id_jenis_kriminalitas"), rs.getInt("id_pelapor"), rs.getString("waktu_kejadian"), rs.getString("lokasi"));
    }
  }

  private int id;
  private int idJenisKriminalitas;
  private int idPelapor;
  private String lokasi;
  private String waktuKejadian;

  public LaporanKriminalO(int id, int id_jenis_kriminalitas, int id_pelapor, String waktu_kejadian, String lokasi) {
    this.id = id;
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.idPelapor = id_pelapor;
    this.lokasi = lokasi;
    this.waktuKejadian = waktu_kejadian;
  }

  public int getId() {
    return id;
  }

  public int getIdJenisKriminalitas() {
    return idJenisKriminalitas;
  }

  public int getIdPelapor() {
    return idPelapor;
  }

  public String getLokasi() {
    return lokasi;
  }

  public String getWaktuKejadian() {
    return waktuKejadian;
  }
}
