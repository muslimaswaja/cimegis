package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JenisKriminalitasO {

  public static class Mapper implements RowMapper<JenisKriminalitasO> {
    @Override public JenisKriminalitasO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new JenisKriminalitasO(rs.getInt("id"), rs.getString("jenis"));
    }
  }

  private int id;
  private String jenis;

  public JenisKriminalitasO(int id, String jenis) {
    this.id = id;
    this.jenis = jenis;
  }
  
  public int getId() {
	  return id;
  }
  
  public String getJenis() {
	  return jenis;
  }
}
