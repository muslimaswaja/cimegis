package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import org.apache.commons.text.WordUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.muslimaswaja.tools.Formatter;

public class LaporanKriminalCsvO {

  public static class Mapper implements RowMapper<LaporanKriminalCsvO> {
    @Override public LaporanKriminalCsvO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new LaporanKriminalCsvO(rs.getString("pelapor"), rs.getString("jenis"), rs.getString("waktu_kejadian"), rs.getString("koordinat"));
    }
  }

  private int no;
  private String pelapor;
  private String jenisKriminalitas;
  private String waktuKejadian;
  private String koordinat;

  public LaporanKriminalCsvO(String pelapor, String jenis, String waktu_kejadian, String koordinat) {
    this.pelapor = pelapor;
    this.jenisKriminalitas = WordUtils.capitalize(jenis);
    this.waktuKejadian = Formatter.formatWaktu(waktu_kejadian);
    this.koordinat = koordinat.replace("POINT(","").replace(")","");
  }

  public int getNo() {
    return no;
  }

  public String getPelapor() {
    return pelapor;
  }

  public String getJenisKriminalitas() {
    return jenisKriminalitas;
  }

  public String getWaktuKejadian() {
    return waktuKejadian;
  }

  public String getKoordinat() {
    return koordinat;
  }

  public void setNo(int no) {
    this.no = no;
  }

  public void setWaktuKejadian(String waktuKejadian) {
    this.waktuKejadian = waktuKejadian;
  }
}
