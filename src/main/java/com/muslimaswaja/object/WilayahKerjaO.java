package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.lang.Math;

public class WilayahKerjaO {

  public static class Mapper implements RowMapper<WilayahKerjaO> {
    @Override public WilayahKerjaO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new WilayahKerjaO(rs.getInt("id"), rs.getString("area"), rs.getString("name"));
    }
  }

  private int id;
  private String area;
  private String nama;

  public WilayahKerjaO(int id, String area, String name) {
    this.id = id;
    this.area = area;
    this.nama = name;
  }

  public int getId() {
    return id;
  }

  public String getArea() {
    return area;
  }

  public String getNama() {
    return nama;
  }
}
