package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JenisLokasiO {

  public static class Mapper implements RowMapper<JenisLokasiO> {
    @Override public JenisLokasiO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new JenisLokasiO(rs.getInt("id"), rs.getString("jenis"));
    }
  }

  private int id;
  private String jenis;

  public JenisLokasiO(int id, String jenis) {
    this.id = id;
    this.jenis = jenis;
  }
  
  public int getId() {
	  return id;
  }
  
  public String getJenis() {
	  return jenis;
  }
}
