package com.muslimaswaja.object;

import java.lang.Math;

public class JarakO implements Comparable<JarakO> {

  private int id;
  private double jarak;

  public JarakO(int id, double jarak) {
    this.id = id;
    this.jarak = jarak;
  }
  
  public int getId() {
	  return id;
  }
  
  public double getJarak() {
	  return jarak;
  }
  
  @Override
  public int compareTo(JarakO o) {
	Double jarakOInteger = (Double) o.getJarak();
	Double jarakInteger = (Double) jarak;
    return jarakInteger.compareTo(jarakOInteger);
  }
}
