package com.muslimaswaja.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ErrorO {

  public static class Mapper implements RowMapper<ErrorO> {
    @Override public ErrorO map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new ErrorO(rs.getInt("id"), rs.getInt("id_jenis_kriminalitas"), rs.getDouble("error"));
    }
  }

  private int id;
  private int idJenisKriminalitas;
  private double error;

  public ErrorO(int id, int id_jenis_kriminalitas, double error) {
    this.id = id;
    this.idJenisKriminalitas = id_jenis_kriminalitas;
    this.error = error;
  }
  
  public int getId() {
	  return id;
  }
  
  public int getIdJenisKriminalitas() {
	  return idJenisKriminalitas;
  }
  
  public double getError() {
	  return error;
  }
  
  public void setId(int id) {
	  this.id = id;
  }
  
  public void setError(double error) {
	  this.error = error;
  }
}
