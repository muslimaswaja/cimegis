package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.util.List;
import java.util.ArrayList;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class KoefisienA extends Jooby {

  {
    path("/api/koefisien", () -> {
		/**
		 * Menambahkan Koefisien dan me-return statusnya.
		 */
		post(req -> {
			KoefisienR db = require(KoefisienR.class);
			KoefisienO koefisien = req.body(KoefisienO.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			int id;
			boolean toReturn = false;

			koefisien.setWaktuInput(sdf.format(timestamp));

			id = db.insert(koefisien);

			if(id > 0) {
				toReturn = true;
			}

			return toReturn;
		});
      
		/**
		 * Daftar Koefisien dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			KoefisienR koefisien = require(KoefisienR.class);

			int start = req.param("start").intValue(0);
			int max = req.param("max").intValue(20);

			return koefisien.list(start,max);
		});
		
		/**
		 * Koefisien berdasarkan ID.
		 */
		get("/:id", req -> {
			KoefisienR db = require(KoefisienR.class);

			int id = req.param("id").intValue();

			KoefisienO koefisien = db.findById(id);

			if(koefisien == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return koefisien;
		});
		
		/**
		 * Update Koefisien berdasarkan ID.
		 */
		put(req -> {
			KoefisienR db = require(KoefisienR.class);
			KoefisienO koefisien = req.body(KoefisienO.class);
			boolean status = false;
			
			if (!db.update(koefisien)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});
		
		/**
		 * Delete Koefisien berdasarkan ID.
		 */
		delete("/:id", req -> {
			KoefisienR db = require(KoefisienR.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	});
  }
}
