package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.util.List;
import java.util.ArrayList;

public class JenisLokasiA extends Jooby {

  {
    path("/api/jenis-lokasi", () -> {
		/**
		 * Menambahkan Jenis Lokasi dan me-return statusnya.
		 */
		post(req -> {
			JenisLokasiR db = require(JenisLokasiR.class);
			JenisLokasiO jenisLokasi = req.body(JenisLokasiO.class);
			int id;
			boolean toReturn = false;

			id = db.insert(jenisLokasi);

			if(id > 0) {
				toReturn = true;
			}

			return toReturn;
		});
      
		/**
		 * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			JenisLokasiR jenisLokasi = require(JenisLokasiR.class);

			int start = req.param("start").intValue(0);
			int max = req.param("max").intValue(20);

			return jenisLokasi.list(start,max);
		});
		
		/**
		 * Jenis Lokasi berdasarkan ID.
		 */
		get("/:id", req -> {
			JenisLokasiR db = require(JenisLokasiR.class);

			int id = req.param("id").intValue();

			JenisLokasiO jenisLokasi = db.findById(id);

			if(jenisLokasi == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return jenisLokasi;
		});
		
		/**
		 * Update Jenis Lokasi berdasarkan ID.
		 */
		put(req -> {
			JenisLokasiR db = require(JenisLokasiR.class);
			JenisLokasiO jenisLokasi = req.body(JenisLokasiO.class);
			boolean status = false;
			
			if (!db.update(jenisLokasi)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});
		
		/**
		 * Delete Jenis Lokasi berdasarkan ID.
		 */
		delete("/:id", req -> {
			JenisLokasiR db = require(JenisLokasiR.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	});
  }
}
