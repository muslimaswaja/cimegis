package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import org.mindrot.jbcrypt.BCrypt;

import java.util.List;
import java.util.ArrayList;

public class UserAccountA extends Jooby {

  {
    path("/api/user-account", () -> {
		/**
		 * Menambahkan User Account dan me-return statusnya.
		 */
		post(req -> {
			UserAccountR db = require(UserAccountR.class);
			UserAccountO userAccount = req.body(UserAccountO.class);
			int id;
			boolean toReturn = false;
			
			
			if(db.isUsernameTaken(userAccount.getUsername()) != 1) {
				userAccount.setPassword(BCrypt.hashpw(
					userAccount.getPassword(),
					BCrypt.gensalt()
				));

				id = db.insert(userAccount);

				if(id > 0) {
					toReturn = true;
				}
			}

			return toReturn;
		});
      
		/**
		 * Daftar User Account dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			UserAccountR userAccount = require(UserAccountR.class);
			int halaman = req.param("halaman").intValue(1);
			String keyword = "%" + req.param("keyword").value("") + "%";
			int start = (halaman-1)*10;
			int max = 10;

			return userAccount.list(start, max, keyword);
		});
		
		/**
		 * User Account berdasarkan ID.
		 */
		get("/:id", req -> {
			UserAccountR db = require(UserAccountR.class);

			int id = req.param("id").intValue();

			UserAccountO userAccount = db.findById(id);

			if(userAccount == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return userAccount;
		});
		
		/**
		 * Update User Account berdasarkan ID.
		 */
		put(req -> {
			UserAccountR db = require(UserAccountR.class);
			UserAccountO userAccount = req.body(UserAccountO.class);
			boolean status = false;
			
			if(db.isUsernameTaken(userAccount.getUsername(), userAccount.getId()) != 1) {
				userAccount.setPassword(
					db.findById(userAccount.getId()).getPassword()
				);
				
				if (!db.update(userAccount)) {
					throw new Err(Status.NOT_FOUND);
				} else {
					status = true;
				}
			}
			
			return status;
		});
		
		/**
		 * Delete User Account berdasarkan ID.
		 */
		delete("/:id", req -> {
			UserAccountR db = require(UserAccountR.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	});
	
    path("/api/jumlah/user", () -> {
		/**
		 * Mendapatkan total jumlah User Account.
		 */
		get(req -> {
			UserAccountR db = require(UserAccountR.class);
			String keyword = "%" + req.param("keyword").value("") + "%";
			int jumlahTotal = db.findTotal(keyword);

			return jumlahTotal;
		});
	});
	
    path("/api/dashboard/user", () -> {
		/**
		 * Mendapatkan 5 User Account untuk ditampilkan di dashboard.
		 */
		get(req -> {
			UserAccountR db = require(UserAccountR.class);

			return db.listForDashboard();
		});
	});
	
    path("/api/max-hal/user", () -> {
		/**
		 * Mendapatkan maksimal halaman User Account.
		 */
		get(req -> {
			UserAccountR db = require(UserAccountR.class);
			String keyword = "%" + req.param("keyword").value("") + "%";
			double jumlahTotal = (double) db.findTotal(keyword);
			double halamanTotal = jumlahTotal/10;
		
			if((halamanTotal%1) > 0) {
				halamanTotal = halamanTotal - (halamanTotal%1) + 1;
			}

			return (int) halamanTotal;
		});
	});
	
    path("/api/ganti-password", () -> {
		/**
		 * Mendapatkan maksimal halaman User Account.
		 */
		put(req -> {
			UserAccountR db = require(UserAccountR.class);
			GantiPasswordO gantiPassword = req.body(GantiPasswordO.class);
			boolean status = false;
			
			gantiPassword.setPassword(BCrypt.hashpw(
				gantiPassword.getPassword(),
				BCrypt.gensalt()
			));
			
			if(db.changePassword(gantiPassword.getPassword(), gantiPassword.getId())) {
				status = true;
			} else {
				throw new Err(Status.NOT_FOUND);
			}
			
			return status;
		});
	});
  }
}
