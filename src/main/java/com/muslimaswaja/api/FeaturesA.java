package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class FeaturesA extends Jooby {

  {
	path("/api/features", () -> {
		/**
		 * Menambahkan FeaturesO dan me-return statusnya.
		 */
		post(req -> {
			FeaturesR db = require(FeaturesR.class);
			FeaturesO features = req.body(FeaturesO.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			int id;
			boolean toReturn = false;

			features.setWaktuInput(sdf.format(timestamp));

			id = db.insert(features);

			if(id > 0) {
				toReturn = true;
			}

			return toReturn;
		});

		/**
		 * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			FeaturesR db = require(FeaturesR.class);

			int halaman = req.param("halaman").intValue(1);
			int idJenisLokasi = req.param("jenis").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			
			if(halaman == 0) {
				halaman = 1;
			}
			
			int start = (halaman-1)*10;
			int max = 10;
			
			if(idJenisLokasi == 0) {
				return db.list(start, max, keyword);
			} else {
				return db.list(start, max, idJenisLokasi, keyword);
			}
		});

		/**
		*
		* Add a new features to the database.
		*/
		get("/:id", req -> {
			FeaturesR db = require(FeaturesR.class);

			int id = req.param("id").intValue();

			FeaturesO features = db.findById(id);

			if(features == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return features;
		});
		
		/**
		 * Update Features berdasarkan ID.
		 */
		put(req -> {
			FeaturesR db = require(FeaturesR.class);
			FeaturesO features = req.body(FeaturesO.class);
			boolean status = false;
			
			if (!db.update(features)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});
		
		/**
		 * Delete Features berdasarkan ID.
		 */
		delete("/:id", req -> {
			FeaturesR db = require(FeaturesR.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	});
	
    path("/api/jumlah/features", () -> {
		/**
		 * Mendapatkan total jumlah Features.
		 */
		get(req -> {
			FeaturesR db = require(FeaturesR.class);
			int idJenisLokasi = req.param("jenis").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			int jumlahTotal;
			
			if(idJenisLokasi == 0) {
				jumlahTotal = db.findTotal(keyword);
			} else {
				jumlahTotal = db.findTotal(idJenisLokasi, keyword);
			}

			return jumlahTotal;
		});
	});
	
    path("/api/dashboard/features", () -> {
		/**
		 * Mendapatkan 5 Features untuk ditampilkan di dashboard.
		 */
		get(req -> {
			FeaturesDashboardR db = require(FeaturesDashboardR.class);

			return db.find();
		});
	});
	
    path("/api/features-terdekat", () -> {
		/**
		 * Mendapatkan features terdekat.
		 */
		get(req -> {
			String koordinat = req.param("koordinat").value("");
			FeaturesR db = require(FeaturesR.class);
			JenisLokasiR jenisLokasiR = require(JenisLokasiR.class);
			List<JenisLokasiO> jenisLokasi = jenisLokasiR.list(0,100);
			List<FeaturesTerdekatO> featuresTerdekat = new ArrayList<>();
			
			for(int i=0; i<jenisLokasi.size(); i++) {
				FeaturesTerdekatO featuresTerdekatDump = db.findTerdekat(jenisLokasi.get(i).getId(), koordinat);
				
				if(featuresTerdekatDump != null) {
					featuresTerdekat.add(featuresTerdekatDump);
				}
			}
			
			Collections.sort(featuresTerdekat);

			return featuresTerdekat;
		});
	});
	
    path("/api/max-hal/features", () -> {
		/**
		 * Mendapatkan maksimal halaman Features.
		 */
		get(req -> {
			FeaturesR db = require(FeaturesR.class);
			int idJenisLokasi = req.param("jenis").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			double jumlahTotal;
			
			if(idJenisLokasi == 0) {
				jumlahTotal = (double) db.findTotal(keyword);
			} else {
				jumlahTotal = (double) db.findTotal(idJenisLokasi, keyword);
			}
			
			double halamanTotal = jumlahTotal/10;
		
			if((halamanTotal%1) > 0) {
				halamanTotal = halamanTotal - (halamanTotal%1) + 1;
			}

			return (int) halamanTotal;
		});
	});
  }
}
