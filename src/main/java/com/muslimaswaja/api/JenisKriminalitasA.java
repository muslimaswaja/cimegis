package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.util.List;
import java.util.ArrayList;

public class JenisKriminalitasA extends Jooby {

  {
    path("/api/jenis-kriminalitas", () -> {
		/**
		 * Menambahkan Jenis Kriminalitas dan me-return id-nya.
		 */
		post(req -> {
			JenisKriminalitasR db = require(JenisKriminalitasR.class);
			JenisKriminalitasO jenisKriminalitas = req.body(JenisKriminalitasO.class);
			int id;
			boolean toReturn = false;

			id = db.insert(jenisKriminalitas);

			if(id > 0) {
				toReturn = true;
			}

			return toReturn;
		});
      
		/**
		 * Daftar Jenis Kriminalitas dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			JenisKriminalitasR jenisKriminalitas = require(JenisKriminalitasR.class);

			int start = req.param("start").intValue(0);
			int max = req.param("max").intValue(20);

			return jenisKriminalitas.list(start,max);
		});
		
		/**
		 * Jenis Kriminalitas berdasarkan ID.
		 */
		get("/:id", req -> {
			JenisKriminalitasR db = require(JenisKriminalitasR.class);

			int id = req.param("id").intValue();

			JenisKriminalitasO jenisKriminalitas = db.findById(id);

			if(jenisKriminalitas == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return jenisKriminalitas;
		});
		
		/**
		 * Update Jenis Kriminalitas berdasarkan ID.
		 */
		put(req -> {
			JenisKriminalitasR db = require(JenisKriminalitasR.class);
			JenisKriminalitasO jenisKriminalitas = req.body(JenisKriminalitasO.class);
			boolean status = false;
			
			if (!db.update(jenisKriminalitas)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});
		
		/**
		 * Delete Jenis Kriminalitas berdasarkan ID.
		 */
		delete("/:id", req -> {
			JenisKriminalitasR db = require(JenisKriminalitasR.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	});
  }
}
