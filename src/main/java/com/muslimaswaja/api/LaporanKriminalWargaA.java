package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;
import com.muslimaswaja.tools.PenghitungKoefisien;
import com.muslimaswaja.tools.JsonReader;
import com.muslimaswaja.tools.RegresiMultivariat;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;

public class LaporanKriminalWargaA extends Jooby {

  {
	path("/api/laporan-kriminal-warga", () -> {
		/**
		 * Menambahkan Laporan kriminal dari warga.
		 */
		post(req -> {
			LaporanKriminalWargaR db = require(LaporanKriminalWargaR.class);
			LaporanKriminalWargaO laporanKriminalWargaO = req.body(LaporanKriminalWargaO.class);
			boolean toReturn = false;
			int id;

			id = db.insert(laporanKriminalWargaO);

			if(id > 0) {
				toReturn = true;
			}

			return toReturn;
		});
		
		/**
		 * Mendapatkan daftar laporan kriminal dari warga.
		 */
		get(req -> {
			LaporanKriminalWargaR db = require(LaporanKriminalWargaR.class);
			
			return db.listAll();
		});
	});
	
	path("/api/cek-validitas", () -> {
		/**
		 * Mengecek kevalidan laporan warga.
		 */
		get(req -> {
			LaporanKriminalWargaR db = require(LaporanKriminalWargaR.class);
			String waktu = req.param("waktu").value();
			String tanggal = req.param("tanggal").value();
			String lokasi = req.param("lokasi").value();
			int jenis = req.param("jenis").intValue();
			boolean toReturn = false;
			
			if(db.cekJumlahLaporanSerupa(jenis, waktu, tanggal, lokasi) == 2) {
				toReturn = true;
			}
			
			return toReturn;
		});
	});
  }
}
