package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;
import com.muslimaswaja.tools.RegresiMultivariat;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MetodeA extends Jooby {

  {
    path("/api/regresi", () -> {
		/**
		 * Mendapatkan hasil penghitungan regresi.
		 */
		get(req -> {
			FeaturesR featuresR = require(FeaturesR.class);
			KonstantaR konstantaR = require(KonstantaR.class);
			KoefisienR koefisienR = require(KoefisienR.class);
			int idJenisKriminalitas = req.param("jenis").intValue(0);
			String koordinat = req.param("koordinat").value("");
			double konstanta = Double.parseDouble(konstantaR.findKonstanta(idJenisKriminalitas));
			List<KoefisienO> koefisien = koefisienR.findByIdJenisKriminalitas(idJenisKriminalitas);
			List<JarakO> jarak = new ArrayList<>();
			
			for(int i=0; i<koefisien.size(); i++) {
				int id = koefisien.get(i).getIdJenisLokasi();
				jarak.add(new JarakO(
					id,
					featuresR.findJarak(id, koordinat)
				));
			}

			return RegresiMultivariat.getRegression(konstanta, koefisien, jarak);
		});
	});
  }
}
