package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;
import com.muslimaswaja.tools.PenghitungKoefisien;
import com.muslimaswaja.tools.JsonReader;
import com.muslimaswaja.tools.RegresiMultivariat;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;

public class LaporanKriminalA extends Jooby {

  {
	path("/api/laporan-kriminal", () -> {
		/**
		 * Menambahkan LaporanKriminalO dan me-return statusnya.
		 */
		post(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			ErrorR dbError = require(ErrorR.class);
			LaporanKriminalO laporanKriminal = req.body(LaporanKriminalO.class);
			int id;
			boolean toReturn = false;

			id = db.insert(laporanKriminal);

			if(id > 0) {
				JenisLokasiR jlRepo = require(JenisLokasiR.class);
				JenisKriminalitasR jkRepo = require(JenisKriminalitasR.class);
				FeaturesR fRepo = require(FeaturesR.class);
				KonstantaR konstantaR = require(KonstantaR.class);
				KoefisienR koefisienR = require(KoefisienR.class);
				List<String> koordinat = db.listForMethod(laporanKriminal.getIdJenisKriminalitas());
				List<Integer> jmlKejadian = new ArrayList<>();
				List<JarakFeaturesO> jarak = new ArrayList<>();
				List<JenisLokasiO> jenisLokasi = jlRepo.list(0,100);
				List<Double> nilaiRegresi = new ArrayList<>();
				
				for(int i=0; i<koordinat.size(); i++) {
					jmlKejadian.add((Integer) db.jumlahLaporanRadius(laporanKriminal.getIdJenisKriminalitas(), koordinat.get(i)));
					nilaiRegresi.add(
						Double.parseDouble(
							JsonReader.readFromURL(
								"http://localhost:8080/api/regresi?jenis="
								+ laporanKriminal.getIdJenisKriminalitas()
								+ "&koordinat="
								+ koordinat.get(i).replace(" ", "%20")
							)
						)
					);
				}
				
				for(int i=0; i<jenisLokasi.size(); i++) {
					List<Double> jarakDump = new ArrayList<>();
					
					for(int j=0; j<koordinat.size(); j++) {
						FeaturesTerdekatO fTerdekat = fRepo.findTerdekat(jenisLokasi.get(i).getId(), koordinat.get(j));
						
						if(fTerdekat != null) {
							jarakDump.add(fTerdekat.getJarakAsli());
						}
					}
					
					if(jarakDump.size() > 0) {
						JarakFeaturesO jarakFeaturesDump = new JarakFeaturesO(jenisLokasi.get(i).getId(), jarakDump);
						jarak.add(jarakFeaturesDump);
					}
				}
				
				List<KoefisienO> koefisien = PenghitungKoefisien.hitungKoefisien(laporanKriminal.getIdJenisKriminalitas(), jmlKejadian, jarak);
				
				koefisienR.delete(laporanKriminal.getIdJenisKriminalitas());
				
				for(int i=0; i<koefisien.size(); i++) {
					if(i==0) {
						if(konstantaR.isExist(koefisien.get(i).getIdJenisKriminalitas()) == 1) {
							konstantaR.update(new KonstantaO(
								konstantaR.findIdByIdJenisKriminalitas(koefisien.get(i).getIdJenisKriminalitas()),
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						} else {
							konstantaR.insert(new KonstantaO(
								0,
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						}
						
						System.out.println("a: " + koefisien.get(i).getNilai());
					} else {
						koefisienR.insert(new KoefisienO(
							0,
							koefisien.get(i).getIdJenisKriminalitas(),
							koefisien.get(i).getNilai(),
							koefisien.get(i).getWaktuInput(),
							koefisien.get(i).getIdJenisLokasi(),
							koefisien.get(i).getKorelasi()
						));
						
						System.out.println("b" + i + ": " + koefisien.get(i).getNilai());
					}
				}
				
				//~ Kalkulasi error
				double[] arrayAsli = new double[jmlKejadian.size()];
				double[] arrayPrediksi = new double[nilaiRegresi.size()];
				
				for(int i=0; i<jmlKejadian.size(); i++) {
					arrayAsli[i] = (double) jmlKejadian.get(i);
				}
				
				for(int i=0; i<nilaiRegresi.size(); i++) {
					arrayPrediksi[i] = (double) nilaiRegresi.get(i);
				}
				
				ErrorO errorO = new ErrorO(
					0,
					laporanKriminal.getIdJenisKriminalitas(),
					RegresiMultivariat.getError(arrayAsli, arrayPrediksi)
				);
				
				System.out.println("Error: " + RegresiMultivariat.getError(arrayAsli, arrayPrediksi));
				
				if(dbError.check(errorO.getIdJenisKriminalitas()) > 0) {
					errorO.setId(dbError.findByIdJenisKriminalitas(laporanKriminal.getIdJenisKriminalitas()).getId());
					boolean isTrue = dbError.update(errorO);
				} else {
					int idNew = dbError.insert(errorO);
				}
				
				toReturn = true;
			}

			return toReturn;
		});

		/**
		 * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			int halaman = req.param("halaman").intValue(1);
			int idJenisKriminalitas = req.param("jenis").intValue(0);
			int idPelapor = req.param("pelapor").intValue(0);
			int idRole = req.param("role").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			
			if(halaman == 0) {
				halaman = 1;
			}
			
			int start = (halaman-1)*10;
			int max = 10;
			
			if(idRole != 0) {
				if(idJenisKriminalitas == 0) {
					return db.listByRole(start, max, idRole, keyword);
				} else {
					return db.listByRole(start, max, idRole, idJenisKriminalitas, keyword);
				}
			} else if(idPelapor != 0) {
				if(idJenisKriminalitas == 0) {
					return db.listByPelapor(start, max, idPelapor, keyword);
				} else {
					return db.listByPelapor(start, max, idPelapor, idJenisKriminalitas, keyword);
				}
			} else {
				if(idJenisKriminalitas == 0) {
					return db.list(start, max, keyword);
				} else {
					return db.list(start, max, idJenisKriminalitas, keyword);
				}
			}
		});

		/**
		*
		* Add a new laporanKriminal to the database.
		*/
		get("/:id", req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);

			int id = req.param("id").intValue();

			LaporanKriminalO laporanKriminal = db.findById(id);

			if(laporanKriminal == null) {
				throw new Err(Status.NOT_FOUND);
			}

			return laporanKriminal;
		});
		
		/**
		 * Update Laporan Kriminal berdasarkan ID.
		 */
		put(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			ErrorR dbError = require(ErrorR.class);
			LaporanKriminalO laporanKriminal = req.body(LaporanKriminalO.class);
			boolean status = false;
			
			if (!db.update(laporanKriminal)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				JenisLokasiR jlRepo = require(JenisLokasiR.class);
				FeaturesR fRepo = require(FeaturesR.class);
				KonstantaR konstantaR = require(KonstantaR.class);
				KoefisienR koefisienR = require(KoefisienR.class);
				List<String> koordinat = db.listForMethod(laporanKriminal.getIdJenisKriminalitas());
				List<Integer> jmlKejadian = new ArrayList<>();
				List<JarakFeaturesO> jarak = new ArrayList<>();
				List<JenisLokasiO> jenisLokasi = jlRepo.list(0,100);
				List<Double> nilaiRegresi = new ArrayList<>();
				
				System.out.println("id jenis kriminalitas: " + laporanKriminal.getIdJenisKriminalitas());
				
				//~ Mencari jumlah kejadian dalam radius 1 kilometer
				for(int i=0; i<koordinat.size(); i++) {
					jmlKejadian.add((Integer) db.jumlahLaporanRadius(laporanKriminal.getIdJenisKriminalitas(), koordinat.get(i)));
					nilaiRegresi.add(
						Double.parseDouble(
							JsonReader.readFromURL(
								"http://localhost:8080/api/regresi?jenis="
								+ laporanKriminal.getIdJenisKriminalitas()
								+ "&koordinat="
								+ koordinat.get(i).replace(" ", "%20")
							)
						)
					);
					
					//~ System.out.println("Koordinat: " + koordinat.get(i));
					//~ System.out.println("y[" + i + "]: " + jmlKejadian.get(i));
					//~ System.out.println();
				}
				
				for(int i=0; i<jenisLokasi.size(); i++) {
					List<Double> jarakDump = new ArrayList<>();
					//~ System.out.println("Jenis Lokasi: " + jenisLokasi.get(i).getId());
					
					for(int j=0; j<koordinat.size(); j++) {
						FeaturesTerdekatO fTerdekat = fRepo.findTerdekat(jenisLokasi.get(i).getId(), koordinat.get(j));
						
						if(fTerdekat != null) {
							jarakDump.add(fTerdekat.getJarakAsli());
							//~ System.out.println("Koordinat: " + koordinat.get(j));
							//~ System.out.println("y[" + j + "]: " + jarakDump.get(j));
						}
					}
					
					//~ System.out.println();
					
					if(jarakDump.size() > 0) {
						JarakFeaturesO jarakFeaturesDump = new JarakFeaturesO(jenisLokasi.get(i).getId(), jarakDump);
						jarak.add(jarakFeaturesDump);
					}
				}
				
				List<KoefisienO> koefisien = PenghitungKoefisien.hitungKoefisien(laporanKriminal.getIdJenisKriminalitas(), jmlKejadian, jarak);
				
				//~ System.out.println("koefisien: " + koefisien.get(1).getKorelasi());
				koefisienR.delete(laporanKriminal.getIdJenisKriminalitas());
				
				for(int i=0; i<koefisien.size(); i++) {
					if(i==0) {
						if(konstantaR.isExist(koefisien.get(i).getIdJenisKriminalitas()) == 1) {
							konstantaR.update(new KonstantaO(
								konstantaR.findIdByIdJenisKriminalitas(koefisien.get(i).getIdJenisKriminalitas()),
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						} else {
							konstantaR.insert(new KonstantaO(
								0,
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						}
					} else {
						koefisienR.insert(new KoefisienO(
							0,
							koefisien.get(i).getIdJenisKriminalitas(),
							koefisien.get(i).getNilai(),
							koefisien.get(i).getWaktuInput(),
							koefisien.get(i).getIdJenisLokasi(),
							koefisien.get(i).getKorelasi()
						));
					}
				}
				
				//~ Kalkulasi error
				double[] arrayAsli = new double[jmlKejadian.size()];
				double[] arrayPrediksi = new double[nilaiRegresi.size()];
				
				System.out.println("dt");
				for(int i=0; i<jmlKejadian.size(); i++) {
					arrayAsli[i] = (double) jmlKejadian.get(i);
					System.out.println(arrayAsli[i]);
				}
				
				System.out.println("d't");
				for(int i=0; i<nilaiRegresi.size(); i++) {
					arrayPrediksi[i] = (double) nilaiRegresi.get(i);
					System.out.println(arrayPrediksi[i]);
				}
				
				ErrorO errorO = new ErrorO(
					0,
					laporanKriminal.getIdJenisKriminalitas(),
					RegresiMultivariat.getError(arrayAsli, arrayPrediksi)
				);
				
				System.out.println("Error: " + RegresiMultivariat.getError(arrayAsli, arrayPrediksi));
				
				if(dbError.check(errorO.getIdJenisKriminalitas()) > 0) {
					errorO.setId(dbError.findByIdJenisKriminalitas(laporanKriminal.getIdJenisKriminalitas()).getId());
					boolean isTrue = dbError.update(errorO);
				} else {
					int idNew = dbError.insert(errorO);
				}
				
				status = true;
			}
			
			return status;
		});
		
		/**
		 * Delete Laporan Kriminal berdasarkan ID.
		 */
		delete("/:id", req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			ErrorR dbError = require(ErrorR.class);
			int id = req.param("id").intValue();
			boolean status = false;
			LaporanKriminalO laporanKriminal = db.findById(id);

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				JenisLokasiR jlRepo = require(JenisLokasiR.class);
				FeaturesR fRepo = require(FeaturesR.class);
				KonstantaR konstantaR = require(KonstantaR.class);
				KoefisienR koefisienR = require(KoefisienR.class);
				List<String> koordinat = db.listForMethod(laporanKriminal.getIdJenisKriminalitas());
				List<Integer> jmlKejadian = new ArrayList<>();
				List<JarakFeaturesO> jarak = new ArrayList<>();
				List<JenisLokasiO> jenisLokasi = jlRepo.list(0,100);
				List<Double> nilaiRegresi = new ArrayList<>();
				
				for(int i=0; i<koordinat.size(); i++) {
					jmlKejadian.add((Integer) db.jumlahLaporanRadius(laporanKriminal.getIdJenisKriminalitas(), koordinat.get(i)));
					nilaiRegresi.add(
						Double.parseDouble(
							JsonReader.readFromURL(
								"http://localhost:8080/api/regresi?jenis="
								+ laporanKriminal.getIdJenisKriminalitas()
								+ "&koordinat="
								+ koordinat.get(i).replace(" ", "%20")
							)
						)
					);
				}
				
				for(int i=0; i<jenisLokasi.size(); i++) {
					List<Double> jarakDump = new ArrayList<>();
					
					for(int j=0; j<koordinat.size(); j++) {
						FeaturesTerdekatO fTerdekat = fRepo.findTerdekat(jenisLokasi.get(i).getId(), koordinat.get(j));
						
						if(fTerdekat != null) {
							jarakDump.add(fTerdekat.getJarakAsli());
						}
					}
					
					if(jarakDump.size() > 0) {
						JarakFeaturesO jarakFeaturesDump = new JarakFeaturesO(jenisLokasi.get(i).getId(), jarakDump);
						jarak.add(jarakFeaturesDump);
					}
				}
				
				List<KoefisienO> koefisien = PenghitungKoefisien.hitungKoefisien(laporanKriminal.getIdJenisKriminalitas(), jmlKejadian, jarak);
				
				System.out.println(koefisien.get(1).getNilai());
				koefisienR.delete(laporanKriminal.getIdJenisKriminalitas());
				
				for(int i=0; i<koefisien.size(); i++) {
					if(i==0) {
						if(konstantaR.isExist(koefisien.get(i).getIdJenisKriminalitas()) == 1) {
							konstantaR.update(new KonstantaO(
								konstantaR.findIdByIdJenisKriminalitas(koefisien.get(i).getIdJenisKriminalitas()),
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						} else {
							konstantaR.insert(new KonstantaO(
								0,
								koefisien.get(i).getIdJenisKriminalitas(),
								koefisien.get(i).getNilai(),
								koefisien.get(i).getWaktuInput()
							));
						}
					} else {
						koefisienR.insert(new KoefisienO(
							0,
							koefisien.get(i).getIdJenisKriminalitas(),
							koefisien.get(i).getNilai(),
							koefisien.get(i).getWaktuInput(),
							koefisien.get(i).getIdJenisLokasi(),
							koefisien.get(i).getKorelasi()
						));
					}
				}
				
				//~ Kalkulasi error
				double[] arrayAsli = new double[jmlKejadian.size()];
				double[] arrayPrediksi = new double[nilaiRegresi.size()];
				
				for(int i=0; i<jmlKejadian.size(); i++) {
					arrayAsli[i] = (double) jmlKejadian.get(i);
				}
				
				for(int i=0; i<nilaiRegresi.size(); i++) {
					arrayPrediksi[i] = (double) nilaiRegresi.get(i);
				}
				
				ErrorO errorO = new ErrorO(
					0,
					laporanKriminal.getIdJenisKriminalitas(),
					RegresiMultivariat.getError(arrayAsli, arrayPrediksi)
				);
				
				System.out.println("Error: " + RegresiMultivariat.getError(arrayAsli, arrayPrediksi));
				
				if(dbError.check(errorO.getIdJenisKriminalitas()) > 0) {
					errorO.setId(dbError.findByIdJenisKriminalitas(laporanKriminal.getIdJenisKriminalitas()).getId());
					boolean isTrue = dbError.update(errorO);
				} else {
					int idNew = dbError.insert(errorO);
				}
				
				status = true;
			}

			return status;
		});
	});
	
	path("/api/jumlah/laporan-kriminal", () -> {
		/**
		 * Mendapatkan total jumlah Laporan Kriminal.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			int idJenisKriminalitas = req.param("jenis").intValue(0);
			int idPelapor = req.param("pelapor").intValue(0);
			int idRole = req.param("role").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			double jumlahTotal;
			
			if(idRole != 0) {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.totalByRole(idRole, keyword);
				} else {
					jumlahTotal = (double) db.totalByRole(idRole, idJenisKriminalitas, keyword);
				}
			} else if(idPelapor != 0) {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.totalByPelapor(idPelapor, keyword);
				} else {
					jumlahTotal = (double) db.totalByPelapor(idPelapor, idJenisKriminalitas, keyword);
				}
			} else {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.total(keyword);
				} else {
					jumlahTotal = (double) db.total(idJenisKriminalitas, keyword);
				}
			}

			return jumlahTotal;
		});
	});
	
	path("/api/max-hal/laporan-kriminal", () -> {
		/**
		 * Mendapatkan maksimal halaman Laporan Kriminal.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			int idJenisKriminalitas = req.param("jenis").intValue(0);
			int idPelapor = req.param("pelapor").intValue(0);
			int idRole = req.param("role").intValue(0);
			String keyword = "%" + req.param("keyword").value("") + "%";
			double jumlahTotal;
			
			if(idRole != 0) {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.totalByRole(idRole, keyword);
				} else {
					jumlahTotal = (double) db.totalByRole(idRole, idJenisKriminalitas, keyword);
				}
			} else if(idPelapor != 0) {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.totalByPelapor(idPelapor, keyword);
				} else {
					jumlahTotal = (double) db.totalByPelapor(idPelapor, idJenisKriminalitas, keyword);
				}
			} else {
				if(idJenisKriminalitas == 0) {
					jumlahTotal = (double) db.total(keyword);
				} else {
					jumlahTotal = (double) db.total(idJenisKriminalitas, keyword);
				}
			}
			
			double halamanTotal = jumlahTotal/10;
		
			if((halamanTotal%1) > 0) {
				halamanTotal = halamanTotal - (halamanTotal%1) + 1;
			}

			return (int) halamanTotal;
		});
	});
	
	path("/api/dashboard/laporan-kriminal", () -> {
		/**
		 * Mendapatkan daftar Laporan Kriminal untuk dashboard.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			
			return db.listForDashboard();
		});
	});
	
	path("/api/heatmap", () -> {
		/**
		 * Mendapatkan titik untuk dibuat heatmap.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			List<HeatmapO> listHeatmap = new ArrayList<>();
			int idJenisKriminalitas = req.param("jenis").intValue(0);
			int bulan = req.param("bulan").intValue(0);
			int waktu = req.param("waktu").intValue(0);
			String waktuAwal = "";
			String waktuAkhir = "";
			
			if(waktu != 0) {
				switch(waktu) {
					case 1: waktuAwal = "00:00:00";
							waktuAkhir = "05:59:59";
							break;
					case 2: waktuAwal = "06:00:00";
							waktuAkhir = "11:59:59";
							break;
					case 3: waktuAwal = "12:00:00";
							waktuAkhir = "17:59:59";
							break;
					case 4: waktuAwal = "18:00:00";
							waktuAkhir = "23:59:59";
							break;
					default: waktuAwal = "00:01:00";
							waktuAkhir = "00:00:00";
							break;
				}
			}
			
			if(idJenisKriminalitas != 0) {
				if(bulan != 0) {
					if(waktu != 0) {
						List<HeatmapO> hDump = db.listAll(idJenisKriminalitas, bulan, waktuAwal, waktuAkhir);
						
						for(HeatmapO hLoop : hDump) {
							listHeatmap.add(hLoop);
						}
					} else {
						List<HeatmapO> hDump = db.listAllByJenisKriminal(idJenisKriminalitas, bulan);
						
						for(HeatmapO hLoop : hDump) {
							listHeatmap.add(hLoop);
						}
					}
				} else {
					if(waktu != 0) {
						List<HeatmapO> hDump =  db.listAllByJenisKriminal(idJenisKriminalitas, waktuAwal, waktuAkhir);
						
						for(HeatmapO hLoop : hDump) {
							listHeatmap.add(hLoop);
						}
					} else {
						List<HeatmapO> hDump =  db.listAllByJenisKriminal(idJenisKriminalitas);
						
						for(HeatmapO hLoop : hDump) {
							listHeatmap.add(hLoop);
						}
					}
				}
			} else if(bulan != 0) {
				if(waktu != 0) {
					List<HeatmapO> hDump =  db.listAllByBulan(bulan, waktuAwal, waktuAkhir);
					
					for(HeatmapO hLoop : hDump) {
						listHeatmap.add(hLoop);
					}
				} else {
					List<HeatmapO> hDump =  db.listAllByBulan(bulan);
					
					for(HeatmapO hLoop : hDump) {
						listHeatmap.add(hLoop);
					}
				}
			} else if(waktu != 0) {
				List<HeatmapO> hDump =  db.listAllByWaktu(waktuAwal, waktuAkhir);
				
				for(HeatmapO hLoop : hDump) {
					listHeatmap.add(hLoop);
				}
			} else {
				List<HeatmapO> hDump =  db.listAll();
				
				for(HeatmapO hLoop : hDump) {
					listHeatmap.add(hLoop);
				}
			}
			
			if(idJenisKriminalitas != 0) {
				double totalBobot = 0.0;
				
				for(int i=0; i<listHeatmap.size(); i++) {
					HasilRegresiO regresiDump = new HasilRegresiO(
						idJenisKriminalitas,
						Double.parseDouble(JsonReader.readFromURL("http://localhost:8080/api/regresi?jenis=" + idJenisKriminalitas + "&koordinat=" + listHeatmap.get(i).getLokasi().replace(" ", "%20")))
					);
					
					double bobotDump = Math.abs(regresiDump.getNilai());
					
					if(bobotDump > 100.0) {
						listHeatmap.get(i).setBobot(100);
					} else {
						listHeatmap.get(i).setBobot(bobotDump);
					}
				}
			}
			
			return listHeatmap;
		});
	});
	
	path("/api/grafik-kriminalitas", () -> {
		/**
		 * Mendapatkan total laporan kriminal dalam radius 1 kilometer.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			ErrorR dbError = require(ErrorR.class);
			JenisKriminalitasR kriminalitas = require(JenisKriminalitasR.class);
			String koordinat = req.param("koordinat").value();
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			Calendar calendar = Calendar.getInstance();
			
			calendar.setTime(new Date(timestamp.getTime()));
			
			int bulan = calendar.get(Calendar.MONTH) + 1;
			List<List> jmlKriminalitas = new ArrayList<>();
			List<JenisKriminalitasO> jnsKriminalitas = kriminalitas.listAll();
			List<HasilRegresiO> hasilRegresi = new ArrayList<>();
			
			for(int i=0;i<jnsKriminalitas.size();i++) {
				HasilRegresiO regresiDump = new HasilRegresiO(
					jnsKriminalitas.get(i).getId(),
					Double.parseDouble(JsonReader.readFromURL("http://localhost:8080/api/regresi?jenis=" + jnsKriminalitas.get(i).getId() + "&koordinat=" + koordinat.replace(" ", "%20")))
				);
				
				System.out.println(regresiDump.getNilai());
				
				hasilRegresi.add(regresiDump);
			}
			
			Collections.sort(hasilRegresi);
			
			for(int i=0; i<hasilRegresi.size();i++) {
				List<JumlahKriminalitasO> jmlKriminalitasDump = new ArrayList<>();
				
				for(int j=0; j<4; j++) {
					JumlahKriminalitasO jko = new JumlahKriminalitasO(hasilRegresi.get(i).getIdJenisKriminalitas(), bulan-j,0, 0);
					ErrorO error = dbError.findByIdJenisKriminalitas(jko.getIdJenisKriminalitas());
					
					if(error != null) {
						jko.setError(error.getError());
					}
					
					jko.setJumlah(db.jumlahLaporanRadius(jko.getIdJenisKriminalitas(), jko.getBulan(), koordinat));
					jmlKriminalitasDump.add(jko);
				}
				
				jmlKriminalitas.add(jmlKriminalitasDump);
			}
			
			return jmlKriminalitas;
		});
	});
	
	path("/api/csv-laporan-kriminalitas", () -> {
		/**
		 * Mendapatkan total laporan kriminal dalam radius 1 kilometer.
		 */
		get(req -> {
			LaporanKriminalR db = require(LaporanKriminalR.class);
			List<LaporanKriminalCsvO> listData = db.listForCSV();
			
			for(int i=0; i<listData.size(); i++) {
				listData.get(i).setNo(i+1);
			}
			
			return listData;
		});
	});
  }
}
