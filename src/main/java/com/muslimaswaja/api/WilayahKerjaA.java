package com.muslimaswaja.api;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;
import com.muslimaswaja.tools.RegresiMultivariat;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class WilayahKerjaA extends Jooby {

  {
	path("/api/wilayah-kerja", () -> {
		/**
		 * Mengambil semua wilayah kerja
		 */
		get(req -> {
			WilayahKerjaR wilayahKerjaR = require(WilayahKerjaR.class);
			
			return wilayahKerjaR.list();
		});
	});
	
    path("/api/cek-wilayah-kerja", () -> {
		/**
		 * Mengecek apakah lokasi dalam wilayah kerja polres tanjung perak
		 */
		get(req -> {
			WilayahKerjaR wilayahKerjaR = require(WilayahKerjaR.class);
			String lokasi = req.param("lokasi").value();
			boolean toReturn = false;
			
			if(lokasi != "") {
				if(wilayahKerjaR.isInWilayahKerja(lokasi) > 0) {
					toReturn = true;
				}
			}
			
			return toReturn;
		});
	});
  }
}
