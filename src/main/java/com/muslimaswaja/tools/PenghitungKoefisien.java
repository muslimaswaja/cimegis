package com.muslimaswaja.tools;

import java.math.BigDecimal;

import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import com.muslimaswaja.object.*;

public class PenghitungKoefisien {
	public static List<KoefisienO> hitungKoefisien(int idKriminalitas, List<Integer> varDependen, List<JarakFeaturesO> varIndependen) {
		List<KoefisienO> koefisien = new ArrayList<>();
		double[] variabelDependen = new double[varDependen.size()];
		double[][] variabelIndependen = new double[varIndependen.size()][varIndependen.get(0).getJarak().size()];
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		//~ System.out.println("Variabel dependen:");
		for(int i=0; i<varDependen.size(); i++) {
			variabelDependen[i] = (double) varDependen.get(i);
			
			//~ System.out.println("Y[" + (i+1) + "]:" + variabelDependen[i]);
		}
		
		PersentaseKorelasi pk = new PersentaseKorelasi(variabelDependen);
		
		for(int i=0; i<varIndependen.size(); i++) {
			double[] varIndependenDump = new double[varIndependen.get(i).getJarak().size()];
			
			for(int j=0; j<varIndependen.get(i).getJarak().size(); j++) {
				varIndependenDump[j] = (double) varIndependen.get(i).getJarak().get(j);
			}
			
			variabelIndependen[i] = varIndependenDump;
			pk.addVarIndependen(varIndependenDump);
		}
		
		//~ pk.printPersentaseKorelasi();
		
		KorelasiMultivariat korelasiMult = new KorelasiMultivariat(
			variabelDependen, variabelIndependen
		);
		RegresiMultivariat regresiMult = new RegresiMultivariat(
			variabelDependen, variabelIndependen
		);
		
		koefisien.add(new KoefisienO(
			0,
			idKriminalitas,
			String.valueOf(regresiMult.getA()),
			sdf.format(timestamp),
			0,
			0
		));
		
		//~ Variabel Dependen
		System.out.println("Variabel Dependen (Y):");
		for(double varDependent : korelasiMult.getVarDependen()) {
			System.out.print(varDependent + " ");
		}
		System.out.println("\nSUM Y: " + korelasiMult.getSumVarDependen());
		System.out.println();
		
		//~ Variabel Independen
		double[][] varIndependent = korelasiMult.getVarIndependen();
		double[] sumVarIndependen = korelasiMult.getSumVarIndependen();
		
		System.out.println("Variabel Independen (Xi):");
		for(int i=0; i<varIndependent.length; i++) {
			System.out.println("X" + (i+1));
			
			for(int j=0; j<varIndependent[i].length; j++) {
				System.out.print(varIndependent[i][j] + " ");
				
			}
			
			System.out.println("\nSUM X" + (i+1) + ": " + sumVarIndependen[i] + "\n");
		}
		System.out.println();
		
		//~ Variabel Dependen Kuadrat
		System.out.println("Variabel Dependen Kuadrat (Y2):");
		for(double varDependenKuadrat : korelasiMult.getVarDependenKuadrat()) {
			System.out.print(varDependenKuadrat + " ");
		}
		System.out.println("\nSUM Y2: " + korelasiMult.getSumVarDependenKuadrat());
		System.out.println();
		
		//~ Variabel Independen Dikali Variabel Independen
		double[][][] xByX = korelasiMult.getXByX();
		double[][] sumXByX = korelasiMult.getSumXByX();
		
		System.out.println("Hasil Kali Variabel Independen (XiXj):");
		for(int i=0; i<xByX.length; i++) {
			for(int j=0; j<xByX[i].length; j++) {
				System.out.println("X" + (i+1) + "X" + (j+1));
				
				for(int k=0; k<xByX[i][j].length; k++) {
					System.out.print(xByX[i][j][k] + " ");
				}
				
				System.out.println("\nSUM X" + (i+1) + "X" + (j+1) + ": " + new BigDecimal(sumXByX[i][j]).toPlainString() + "\n");
				System.out.println();
			}
		}
		
		//~ Variabel Independen Dikali Variabel Dependen
		double[][] xByY = korelasiMult.getXByY();
		double[] sumXByY = korelasiMult.getSumXByY();
		
		System.out.println("Hasil Kali Variabel Independen Dengan Variabel Dependen (XiY):");
		for(int i=0; i<xByY.length; i++) {
			System.out.println("X" + (i+1) + "Y");
			
			for(int j=0; j<xByY[i].length; j++) {
				System.out.print(xByY[i][j] + " ");
			}
			
			System.out.println("\nSUM X" + (i+1) + "Y: " + sumXByY[i] + "\n");
		}
		System.out.println();
		
		//~ Variabel Independen Dikali Variabel Dependen untuk Penghitungan Korelasi
		System.out.println("Hasil Kali Variabel Independen Dengan Variabel Dependen (XiY) Real:");
		for(double realSumXByY : korelasiMult.getRealSumXByY()) {
			System.out.print(realSumXByY + " ");
		}
		System.out.println();
		System.out.println();
		
		//~ System.out.println("korelasi sederhana:");
		//~ for(int i=0; i<korelasiMult.getKorelasiSederhana().length; i++) {
			//~ System.out.println("X[" + (i+1) + "]: " + korelasiMult.getKorelasiSederhana()[i]);
		//~ }
		
		//~ System.out.println("Model regresi multivariat:");
		//~ System.out.print("Y = ");
		//~ System.out.print(regresiMult.getA());
		
		for(int i=0; i<regresiMult.getB().size(); i++) {
			koefisien.add(new KoefisienO(
				0,
				idKriminalitas,
				BigDecimal.valueOf(regresiMult.getB().get(i)).toPlainString(),
				sdf.format(timestamp),
				varIndependen.get(i).getId(),
				korelasiMult.getKorelasiSederhana()[i]
			));
			
			//~ System.out.print(" + (" + regresiMult.getB().get(i) + ")X" + (i+1));
		}
		
		//~ System.out.println();
		
		return koefisien;
	}
}
