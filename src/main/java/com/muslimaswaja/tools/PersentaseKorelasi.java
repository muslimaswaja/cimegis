package com.muslimaswaja.tools;

import java.lang.Math;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class PersentaseKorelasi {
	private int i;
	private int j;
	private int n;
	private ArrayList<Double> varDependen = new ArrayList<Double>();
	private ArrayList<ArrayList<Double>> varIndependen = new ArrayList<ArrayList<Double>>();
	private ArrayList<Double> korelasi = new ArrayList<Double>();
	private ArrayList<Double> persentaseKorelasi = new ArrayList<Double>();
	
	public PersentaseKorelasi(double[] varDependen) {
		for(i=0; i<varDependen.length; i++) {
			this.varDependen.add(varDependen[i]);
		}
	}
	
	public void addVarIndependen(double[] varIndependen) {
		ArrayList<Double> varIndDump = new ArrayList<Double>();
		Pearson korelasi = new Pearson(varIndependen, this.getVarDependen());
		
		for(i=0; i<varIndependen.length; i++) {
			varIndDump.add(varIndependen[i]);
		}
		
		if(this.varIndependen.size() == 0) {
			n = varIndependen.length;
		} else if(n > varIndependen.length) {
			n = varIndependen.length;
		}
		
		this.varIndependen.add(varIndDump);
		this.korelasi.add(korelasi.getR());
		this.calculatePercent();
	}
	
	public double[] getVarDependen() {
		double[] toReturn = new double[varDependen.size()];
		
		for(i=0; i<varDependen.size(); i++) {
			toReturn[i] = Double.parseDouble(
				varDependen.get(i).toString()
			);
		}
		
		return toReturn;
	}
	
	public double[][] getVarIndependen(int parameter) {
		double[][] toReturn = new double[varIndependen.size()][n];
		
		if(parameter == 0) { // Ambil semua variabel independen
			for(i=0; i<varIndependen.size(); i++) {
				for(j=0; j<n; j++) {
					toReturn[i][j] = Double.parseDouble(
						varIndependen.get(i).get(j).toString()
					);
				}
			}
		} else if(parameter == 1) { // Ambil parameter yang signifikan saja
			int[] usedIndex = this.getUsedIndex();
			toReturn = new double[usedIndex.length][n];
			int index = 0;
			
			for(i=0; i<varIndependen.size(); i++) {
				if(IntStream.of(usedIndex).anyMatch(x -> x == i)) {
					for(j=0; j<n; j++) {
						toReturn[index][j] = Double.parseDouble(
							varIndependen.get(i).get(j).toString()
						);
					}
					
					index++;
				}
			}
		}
		
		return toReturn;
	}
	
	private void calculatePercent() {
		double jmlKorelasi = 0;
		
		for(i=0; i<korelasi.size(); i++) {
			jmlKorelasi += Math.abs(Double.parseDouble(
				korelasi.get(i).toString()
			));
		}
		
		persentaseKorelasi.clear();
		
		for(i=0; i<korelasi.size(); i++) {
			persentaseKorelasi.add(
				(Double.parseDouble(korelasi.get(i).toString())
				/ jmlKorelasi) * 100
			);
		}
	}
	
	public void printVarDependen() {
		for(i=0; i<varDependen.size(); i++) {
			//~ System.out.print(this.varDependen.get(i) + " ");
		}
		
		System.out.println();
	}
	
	public void printKorelasi() {
		for(i=0; i<korelasi.size(); i++) {
			//~ System.out.print(this.korelasi.get(i) + " ");
		}
		
		System.out.println();
	}
	
	public void printVarIndependen() {
		for(i=0; i<varIndependen.size(); i++) {
			for(j=0; j<varIndependen.get(i).size(); j++) {	
				//~ System.out.print(this.varIndependen.get(i).get(j) + " ");
			}
			
			//~ System.out.println();
		}
		
		//~ System.out.println();
	}
	
	public void printPersentaseKorelasi() {
		for(i=0; i<persentaseKorelasi.size(); i++) {
			//~ System.out.print(this.persentaseKorelasi.get(i) + "% ");
		}
		
		//~ System.out.println();
	}
	
	public void printUsedKorelasi() {
		for(i=0; i<persentaseKorelasi.size(); i++) {
			if(Math.abs(Double.parseDouble(
				this.persentaseKorelasi.get(i).toString())) > 5
			) {
				//~ System.out.print(this.persentaseKorelasi.get(i) + "% ");
			}
		}
		
		//~ System.out.println();
	}
	
	public int[] getUsedIndex() {
		int[] toReturn;
		ArrayList<Integer> usedIndex = new ArrayList<Integer>();
		
		for(i=0; i<persentaseKorelasi.size(); i++) {
			if(Math.abs(Double.parseDouble(
				this.persentaseKorelasi.get(i).toString())) > 5
			) {
				usedIndex.add(i);
			}
		}
		
		toReturn = new int[usedIndex.size()];
		
		for(i=0; i<usedIndex.size(); i++) {
			toReturn[i] = Integer.parseInt(usedIndex.get(i).toString());
		}
		
		return toReturn;
	}
}
