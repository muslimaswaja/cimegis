package com.muslimaswaja.tools;

import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.Session;

public class Routes extends Jooby {

  {
	cookieSession();
	use(new FlashScope());
	
	//~ User
	get("/", req -> Results.html("index"));
	get("/bantuan", req -> Results.html("bantuan"));
	
	//~ Sudo
	get("/sudo/login", req -> Results.html("sudo/login"));
	
	get("/sudo", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/dashboard");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/bantuan", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/bantuan");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/pengaturan", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/setting");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/lokasi", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/list-features");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/lokasi/tambah", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/add-features");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/user", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			return Results.html("sudo/list-user");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	get("/sudo/user/tambah", req -> {
		Session session = req.session();
		
		if(session.isSet("ids")) {
			req.set("session", session);
			req.flash("tambah", "true");
			return Results.redirect("/sudo/user");
		} else {
			return Results.redirect("/sudo/login");
		}
	});
	
	//~ Admin
	get("/admin/login", req -> Results.html("admin/login"));
	
	get("/admin", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/dashboard");
		} else {
			return Results.redirect("/admin/login");
		}
	});
	get("/admin/pengaturan", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/setting");
		} else {
			return Results.redirect("/admin/login");
		}
	});
	get("/admin/hotspot", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/hotspot");
		} else {
			return Results.redirect("/admin/login");
		}
	});
	get("/admin/laporan", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/list-laporan");
		} else {
			return Results.redirect("/admin/login");
		}
	});
	get("/admin/laporan/tambah", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/add-laporan");
		} else {
			return Results.redirect("/admin/login");
		}
	});
	get("/admin/bantuan", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			return Results.html("admin/bantuan");
		} else {
			return Results.redirect("/admin/login");
		}
	});
  }
}
