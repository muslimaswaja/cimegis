package com.muslimaswaja.tools;

public class Formatter {
	public static String formatWaktu(String waktuAsal) {
		String[] waktu = waktuAsal.split(" ");
		String jam = formatJam(waktu[1]);
		String tanggal = formatTanggal(waktu[0]);
		String waktuBaru = jam + ", " + tanggal;
		
		return waktuBaru;
	}
	
	public static String formatJam(String jam) {
		return jam.substring(0,5);
	}
	
	public static String formatTanggal(String tanggalAsal) {
		String[] tanggal = tanggalAsal.split("-");
		String tanggalBaru = tanggal[2] + " " + formatBulan(Integer.parseInt(tanggal[1])) + " " + tanggal[0];
		
		return tanggalBaru;
	}
	
	public static String formatBulan(int angkaBulan) {
		String namaBulan;
		
		switch (angkaBulan) {
			case 1:
				namaBulan = "Januari";
				break;
			case 2:
				namaBulan = "Februari";
				break;
			case 3:
				namaBulan = " Maret";
				break;
			case 4:
				namaBulan = "April";
				break;
			case 5:
				namaBulan = " Mei";
				break;
			case 6:
				namaBulan = "Juni";
				break;
			case 7:
				namaBulan = "Juli";
				break;
			case 8:
				namaBulan = "Agustus";
				break;
			case 9:
				namaBulan = "September";
				break;
			case 10:
				namaBulan = "Oktober";
				break;
			case 11:
				namaBulan = "November";
				break;
			case 12:
				namaBulan = " Desember";
				break;
			default:
				namaBulan = "Nama bulan tidak ditemukan!";
		}
		
		return namaBulan;
	}
}
