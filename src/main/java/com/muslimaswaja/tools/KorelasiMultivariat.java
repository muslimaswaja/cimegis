package com.muslimaswaja.tools;

import java.lang.Math;
import java.math.BigDecimal;
import java.util.ArrayList;

public class KorelasiMultivariat {
	private double n;
	private double sumVarDependen = 0;
	private double sumVarDependenKuadrat = 0;
	private double realSumY = 0;
	private ArrayList<Double> varDependen = new ArrayList<Double>();
	private ArrayList<Double> varDependenKuadrat = new ArrayList<Double>();
	private ArrayList<Double> sumVarIndependen = new ArrayList<Double>();
	private ArrayList<Double> sumVarIndependenKuadrat = new ArrayList<Double>();
	private ArrayList<Double> sumXByY = new ArrayList<Double>();
	private ArrayList<Double> realSumXByY = new ArrayList<Double>();
	private ArrayList<Double> realSumXiByXi = new ArrayList<Double>();
	private ArrayList<Double> korelasiSederhana = new ArrayList<Double>();
	private ArrayList<ArrayList<Double>> varIndependen = new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<Double>> varIndependenKuadrat = new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<Double>> sumXByX = new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<Double>> xByY = new ArrayList<ArrayList<Double>>();
	private ArrayList<ArrayList<ArrayList<Double>>> xByX = new ArrayList<ArrayList<ArrayList<Double>>>();
	
	//~ Constructor
	public KorelasiMultivariat(double[] varDependen, double[][] varIndependen) {
		this.n = (double) varDependen.length;
		
		System.out.println("N: " + this.n);
		
		for(int i=0; i<varDependen.length; i++) {
			this.varDependen.add(varDependen[i]);
			this.varDependenKuadrat.add(Math.pow(varDependen[i], 2));
			
			//~ System.out.println("varDependen[" + i + "]: " + this.varDependen.get(i));
			//~ System.out.println("varDependenKuadrat[" + i + "]: " + this.varDependenKuadrat.get(i));
			
			this.sumVarDependen += varDependen[i];
			this.sumVarDependenKuadrat += (double) varDependenKuadrat.get(i);
		}
		
		for(int i=0; i<varIndependen.length; i++) {
			ArrayList<Double> varIndDump = new ArrayList<Double>();
			double sumVarIndependenDump = 0;
			
			for(int j=0; j<varIndependen[i].length; j++) {
				varIndDump.add(varIndependen[i][j]);
				//~ System.out.println("varIndependen[" + i + "][" + j + "]: " + varIndependen[i][j]);
				
				sumVarIndependenDump += varIndependen[i][j];
			}
			
			this.varIndependen.add(varIndDump);
			this.sumVarIndependen.add(sumVarIndependenDump);
		}
		
		multiplyXByX();
		multiplyXByY();
		calculateRealSumXByY();
		calculateRealSumXiByXi();
		calculateRealSumY();
		calculateKorelasiSederhana();
	}
	
	//~ Calculator
	private void multiplyXByX() {
		for(int i=0; i<varIndependen.size(); i++) {
			ArrayList<ArrayList<Double>> xByXDumpOuter = new ArrayList<ArrayList<Double>>();
			ArrayList<Double> sumXByXDumpOuter = new ArrayList<Double>();
			
			for(int j=0; j<varIndependen.size(); j++) {
				ArrayList<Double> xByXDumpInner = new ArrayList<Double>();
				double sumXByXInnerDump = 0;
				
				for(int k=0; k<varIndependen.get(i).size(); k++) {
					xByXDumpInner.add(
						((double) varIndependen.get(i).get(k))
						* ((double) varIndependen.get(j).get(k))
					);
					
					sumXByXInnerDump += (double) xByXDumpInner.get(k);
				}
				
				xByXDumpOuter.add(xByXDumpInner);
				sumXByXDumpOuter.add(sumXByXInnerDump);
			}
			
			xByX.add(xByXDumpOuter);
			sumXByX.add(sumXByXDumpOuter);
		}
		
		System.out.println(xByX.get(0).size());
	}
	
	private void multiplyXByY() {
		for(int i=0; i<varIndependen.size(); i++) {
			ArrayList<Double> xByYDump = new ArrayList<Double>();
			double sumXByYDump = 0;
			
			for(int j=0; j<varIndependen.get(i).size(); j++) {
				xByYDump.add(
					((double) varIndependen.get(i).get(j))
					* ((double) varDependen.get(j))
				);
				
				sumXByYDump += (double) xByYDump.get(j);
			}
			
			xByY.add(xByYDump);
			sumXByY.add(sumXByYDump);
		}
		
		System.out.println(xByX.get(0).size());
	}
	
	public void calculateRealSumXByY() {
		for(int i=0; i<sumXByY.size(); i++) {
			realSumXByY.add(((double) sumXByY.get(i)) - ((
				(double) sumVarIndependen.get(i) * sumVarDependen)
			/ n));
		}
	}
	
	public void calculateRealSumXiByXi() {
		for(int i=0; i<sumXByX.size(); i++) {
			realSumXiByXi.add(((double) sumXByX.get(i).get(i)) - ((
				Math.pow((double) sumXByY.get(i), 2))
			/ n));
		}
	}
	
	public void calculateRealSumY() {
		realSumY = sumVarDependenKuadrat
			- (Math.pow(sumVarDependen, 2) / n);
	}
	
	public void calculateKorelasiSederhana() {
		for(int i=0; i<realSumXByY.size(); i++) {
			korelasiSederhana.add(((double) realSumXByY.get(i))
				/ Math.sqrt(Math.abs(((double) realSumXiByXi.get(i) * realSumY))));
		}
	}
	
	//~ Getter
	public int getN() {
		return (int) n;
	}
	
	public double[] getVarDependen() {
		double[] toReturn = new double[varDependen.size()];
		
		for(int i=0; i<varDependen.size(); i++) {
			toReturn[i] = (double) varDependen.get(i);
		}
		
		return toReturn;
	}
	
	public double[] getVarDependenKuadrat() {
		double[] toReturn = new double[varDependenKuadrat.size()];
		
		for(int i=0; i<varDependenKuadrat.size(); i++) {
			toReturn[i] = (double) varDependenKuadrat.get(i);
		}
		
		return toReturn;
	}
	
	public double getSumVarDependen() {
		return sumVarDependen;
	}
	
	public double getSumVarDependenKuadrat() {
		return sumVarDependenKuadrat;
	}
	
	public double[][] getVarIndependen() {
		double[][] toReturn = new double[varIndependen.size()][varIndependen.get(0).size()];
		
		for(int i=0; i<varIndependen.size(); i++) {
			for(int j=0; j<varIndependen.get(i).size(); j++) {
				toReturn[i][j] = (double) varIndependen.get(i).get(j);
			}
		}
		
		return toReturn;
	}
	
	public double[] getSumVarIndependen() {
		double[] toReturn = new double[sumVarIndependen.size()];
		
		for(int i=0; i<sumVarIndependen.size(); i++) {
			toReturn[i] = (double) sumVarIndependen.get(i);
		}
		
		return toReturn;
	}
	
	public double[][][] getXByX() {
		double[][][] toReturn = new double[xByX.size()][xByX.get(0).size()][xByX.get(0).get(0).size()];
		
		for(int i=0; i<xByX.size(); i++) {
			for(int j=0; j<xByX.get(i).size(); j++) {
				for(int k=0; k<xByX.get(i).get(j).size(); k++) {
					toReturn[i][j][k] = (double) xByX.get(i).get(j).get(k);
				}
			}
		}
		
		return toReturn;
	}
	
	public double[][] getSumXByX() {
		double[][] toReturn = new double[sumXByX.size()][sumXByX.get(0).size()];
		
		for(int i=0; i<sumXByX.size(); i++) {
			for(int j=0; j<sumXByX.get(i).size(); j++) {
				toReturn[i][j] = (double) sumXByX.get(i).get(j);
			}
		}
		
		return toReturn;
	}
	
	public double[][] getXByY() {
		double[][] toReturn = new double[xByY.size()][xByY.get(0).size()];
		
		for(int i=0; i<xByY.size(); i++) {
			for(int j=0; j<xByY.get(i).size(); j++) {
				toReturn[i][j] = (double) xByY.get(i).get(j);
			}
		}
		
		return toReturn;
	}
	
	public double[] getSumXByY() {
		double[] toReturn = new double[sumXByY.size()];
		
		for(int i=0; i<sumXByY.size(); i++) {
			toReturn[i] = (double) sumXByY.get(i);
		}
		
		return toReturn;
	}
	
	public double[] getRealSumXByY() {
		double[] toReturn = new double[realSumXByY.size()];
		
		for(int i=0; i<realSumXByY.size(); i++) {
			toReturn[i] = (double) realSumXByY.get(i);
		}
		
		return toReturn;
	}
	
	public double[] getRealSumXiByXi() {
		double[] toReturn = new double[realSumXiByXi.size()];
		
		for(int i=0; i<realSumXiByXi.size(); i++) {
			toReturn[i] = (double) realSumXiByXi.get(i);
		}
		
		return toReturn;
	}
	
	public double getRealSumY() {
		return realSumY;
	}
	
	public double[] getKorelasiSederhana() {
		double[] toReturn = new double[korelasiSederhana.size()];
		
		for(int i=0; i<korelasiSederhana.size(); i++) {
			//~ System.out.println("korelasi sederhana[" + i + "]: " + korelasiSederhana.get(i));
			toReturn[i] = (double) korelasiSederhana.get(i);
		}
		
		return toReturn;
	}
}

