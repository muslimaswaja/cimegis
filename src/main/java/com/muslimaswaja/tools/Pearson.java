package com.muslimaswaja.tools;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

class Pearson {
	private int i;
	private int n;
	private double[] sigma = new double[5];
	private double r;
	
	private ArrayList x = new ArrayList<Double>();
	private ArrayList y = new ArrayList<Double>();
	private ArrayList xy = new ArrayList<Double>();
	private ArrayList kuadratX = new ArrayList<Double>();
	private ArrayList kuadratY = new ArrayList<Double>();
	
	public Pearson(double[] x, double[] y) {
		fillX(x);
		fillY(y);
	}
	
	private void setN() {
		if(x.size() < y.size() || x.size() == y.size()) {
			n = x.size();
		} else {
			n = y.size();
		}
	}
	
	private void addX(double x) {
		this.x.add(x);
		setN();
		calculateR();
	}
	
	private void addY(double Y) {
		this.y.add(y);
		setN();
		calculateR();
	}
	
	private void fillX(double[] x) {
		for(i=0; i<x.length; i++) {
			this.x.add(x[i]);
		}
		
		setN();
		calculateR();
	}
	
	private void fillY(double[] y) {
		for(i=0; i<y.length; i++) {
			this.y.add(y[i]);
		}
		
		setN();
		calculateR();
	}
	
	private void calculateR() {
		double sOfX = 0;
		double sOfY = 0;
		double sOfXY = 0;
		double sOfKX = 0;
		double sOfKY = 0;
		double pemb, peny;
		
		for(int i=0; i < n; i++) {
			xy.add(Double.parseDouble(x.get(i).toString())
				* Double.parseDouble(y.get(i).toString()));
			kuadratX.add(pow(Double.parseDouble(x.get(i).toString()),2));
			kuadratY.add(pow(Double.parseDouble(y.get(i).toString()),2));
			
			sOfX += Double.parseDouble(x.get(i).toString());
			sOfY += Double.parseDouble(y.get(i).toString());
			sOfXY += Double.parseDouble(xy.get(i).toString());
			sOfKX += Double.parseDouble(kuadratX.get(i).toString());
			sOfKY += Double.parseDouble(kuadratY.get(i).toString());
		}
		
		sigma[0] = sOfX;
		sigma[1] = sOfY;
		sigma[2] = sOfXY;
		sigma[3] = sOfKX;
		sigma[4] = sOfKY;
		
		pemb = (n * sigma[2]) - (sigma[0] * sigma[1]);
		peny = sqrt(((n * sigma[3]) - pow(sigma[0], 2)) * ((n * sigma[4]) - pow(sigma[1], 2)));
		r = pemb / peny;
	}
	
	public int getN() {
		return n;
	}
	
	public double[] getX() {
		double[] x = new double[this.x.size()];
		
		for(i=0; i<this.x.size(); i++) {
			x[i] = Double.parseDouble(this.x.get(i).toString());
		}
		
		return x;
	}
	
	public double[] getY() {
		double[] y = new double[this.y.size()];
		
		for(i=0; i<this.y.size(); i++) {
			y[i] = Double.parseDouble(this.y.get(i).toString());
		}
		
		return y;
	}
	
	public double[] getXY() {
		double[] xy = new double[this.xy.size()];
		
		for(i=0; i<this.xy.size(); i++) {
			xy[i] = Double.parseDouble(this.xy.get(i).toString());
		}
		
		return xy;
	}
	
	public double[] getKuadratX() {
		double[] kuadratX = new double[this.kuadratX.size()];
		
		for(i=0; i<this.kuadratX.size(); i++) {
			kuadratX[i] = Double.parseDouble(this.kuadratX.get(i).toString());
		}
		
		return kuadratX;
	}
	
	public double[] getKuadratY() {
		double[] kuadratY = new double[this.kuadratY.size()];
		
		for(i=0; i<this.kuadratY.size(); i++) {
			kuadratY[i] = Double.parseDouble(this.kuadratY.get(i).toString());
		}
		
		return kuadratY;
	}
	
	public double getSumOfX() {
		return sigma[0];
	}
	
	public double getSumOfY() {
		return sigma[1];
	}
	
	public double getSumOfXY() {
		return sigma[2];
	}
	
	public double getSumOfKuadratX() {
		return sigma[3];
	}
	
	public double getSumOfKuadratY() {
		return sigma[4];
	}
	
	public double getR() {
		return r;
	}
}
