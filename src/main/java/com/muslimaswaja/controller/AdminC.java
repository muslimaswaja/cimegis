package com.muslimaswaja.controller;

import com.muslimaswaja.repository.*;
import com.muslimaswaja.object.*;

import org.jooby.Err;
import org.jooby.FlashScope;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.Session;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

import org.mindrot.jbcrypt.BCrypt;

public class AdminC extends Jooby {

  {
	cookieSession();
	use(new FlashScope());
	
	//~ Login
	post("/admin/login/do", req -> {
		UserAccountR db = require(UserAccountR.class);
		LoginO login = req.form(LoginO.class);

		String username = login.getUsername();
		String password = login.getPassword();

		String realPassword = db.findAdminPassword(username);
		
		if(realPassword != null) {
			if(BCrypt.checkpw(password, realPassword)) {
				UserAccountO user = db.findByPassword(realPassword);
				Session session = req.session();
				
				session.set("ida", user.getId());
				session.set("nama", user.getNama());
				
				return Results.redirect("/admin");
			}
		}
		
		req.flash("gagal", "true");
		req.flash("username", username);

		return Results.redirect("/admin/login");
	});
	
	//~ Logout
	get("/admin/logout", req -> {
		Session session = req.session();
	
		if(session.isSet("ida")) {
			session.unset();
		}
		
		return Results.redirect("/admin");
	});
	
	//~ Notif berhasil
	get("/admin/laporan/sukses", req -> {
		Session session = req.session();
		
		if(session.isSet("ida")) {
			req.set("session", session);
			req.flash("sukses", "true");
			return Results.redirect("/admin/laporan");
		} else {
			return Results.redirect("/admin/login");
		}
	});
  }
}
