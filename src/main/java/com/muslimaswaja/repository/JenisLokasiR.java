package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel jenis_lokasi.
 */
@RegisterRowMapper(JenisLokasiO.Mapper.class)
public interface JenisLokasiR {
  /**
   * Daftar semua Jenis Lokasi.
   */
  @SqlQuery("SELECT * FROM jenis_lokasi")
  List<JenisLokasiO> listAll();
  
  /**
   * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM jenis_lokasi limit :max offset :start")
  List<JenisLokasiO> list(int start, int max);

  /**
   * Jenis Lokasi berdasarkan ID.
   */
 @SqlQuery("SELECT * FROM jenis_lokasi WHERE id=:id")
 JenisLokasiO findById(int id);

  /**
   * Menambahkan Jenis Lokasi dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO jenis_lokasi(jenis) values(:jenis)")
  @GetGeneratedKeys
  int insert(@BindBean JenisLokasiO JenisLokasiO);

  /**
   * Meng-update Jenis Lokasi berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE jenis_lokasi SET jenis=:jenis WHERE id=:id")
  boolean update(@BindBean JenisLokasiO JenisLokasiO);

  /**
   * Menghapus Jenis Lokasi berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM jenis_lokasi WHERE id=:id")
  boolean delete(int id);
}
