package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel laporan_kriminal.
 */
@RegisterRowMapper(LaporanKriminalWargaO.Mapper.class)
public interface LaporanKriminalWargaR {
	/**
	* Daftar semua laporan warga.
	*/
	@SqlQuery("SELECT id, id_jenis_kriminalitas, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal_warga")
	List<LaporanKriminalWargaO> listAll();

	/**
	* Laporan Kriminal Warga berdasarkan ID.
	*/
	@SqlQuery("SELECT id, id_jenis_kriminalitas, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal_warga WHERE id=:id")
	LaporanKriminalO findById(int id);

	/**
	* Cek jumlah laporan yang serupa.
	*/
	@SqlQuery(
		"WITH rentang AS ("
			+ "SELECT waktu_kejadian::time-to_timestamp(:waktu,'HH24:MI:MS')::time AS rentang "
			+ "FROM laporan_kriminal_warga"
		+ ") "
		+ "SELECT COUNT(*) "
		+ "FROM laporan_kriminal_warga AS lkw "
		+ "WHERE lkw.id_jenis_kriminalitas=:jenis "
		+ "AND lkw.waktu_kejadian::date = to_timestamp(:tanggal, 'YYYY-MM-DD')::date "
		+ "AND ST_Distance(ST_transform(lokasi, 26986), ST_Transform(ST_GeomFromText(:lokasi, 4326), 26986))\\<100"
		+ "AND greatest("
		+ "waktu_kejadian::time-to_timestamp(:waktu,'HH24:MI:MS')::time,"
		+ "-(waktu_kejadian::time-to_timestamp(:waktu,'HH24:MI:MS')::time)"
		+ ") \\<= to_timestamp('01:00:00','HH24:MI:MS')::time "
	)
	int cekJumlahLaporanSerupa(int jenis, String waktu, String tanggal, String lokasi);

	/**
	* Menambahkan Laporan Kriminal dan me-return keberhasilannya.
	*/
	@SqlUpdate("INSERT INTO laporan_kriminal_warga(id_jenis_kriminalitas, lokasi, waktu_kejadian) values(:idJenisKriminalitas, ST_GeomFromText(:lokasi,4326), to_timestamp(:waktuKejadian, 'YYYY-MM-DD HH24:MI:MS'))")
	@GetGeneratedKeys
	int insert(@BindBean LaporanKriminalWargaO LaporanKriminalWargaO);
}
