package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel features.
 */
@RegisterRowMapper(FeaturesDashboardO.Mapper.class)
public interface FeaturesDashboardR {
 /**
   * Total Features berdasarkan kategori.
   */
 @SqlQuery("SELECT jl.jenis, COUNT(*) AS jumlah FROM features AS f, jenis_lokasi AS jl WHERE f.id_jenis_lokasi=jl.id GROUP BY jl.jenis ORDER BY jumlah DESC LIMIT 5")
 List<FeaturesDashboardO> find();
}
