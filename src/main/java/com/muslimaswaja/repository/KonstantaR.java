package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel konstanta.
 */
@RegisterRowMapper(KonstantaO.Mapper.class)
public interface KonstantaR {
  /**
   * Daftar Konstanta dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM konstanta limit :max offset :start")
  List<KonstantaO> list(int start, int max);

  /**
   * Konstanta berdasarkan ID.
   */
 @SqlQuery("SELECT * FROM konstanta WHERE id=:id")
 KonstantaO findById(int id);

  /**
   * Nilai konstanta berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT nilai FROM konstanta WHERE id_jenis_kriminalitas=:idJenisKriminalitas ORDER BY id DESC LIMIT 1")
 String findKonstanta(int idJenisKriminalitas);

  /**
   * ID konstanta berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT id FROM konstanta WHERE id_jenis_kriminalitas=:idJenisKriminalitas LIMIT 1")
 int findIdByIdJenisKriminalitas(int idJenisKriminalitas);

  /**
   * Cek keberadaan konstanta berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT COUNT(*) FROM konstanta WHERE id_jenis_kriminalitas=:idJenisKriminalitas")
 int isExist(int idJenisKriminalitas);

  /**
   * Menambahkan Konstanta dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO konstanta(id_jenis_kriminalitas, nilai, waktu_input) values(:idJenisKriminalitas, :nilai, to_timestamp(:waktuInput, 'YYYY-MM-DD HH24:MI:MS'))")
  @GetGeneratedKeys
  int insert(@BindBean KonstantaO KonstantaO);

  /**
   * Meng-update Konstanta berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE konstanta SET id_jenis_kriminalitas=:idJenisKriminalitas, nilai=:nilai WHERE id=:id")
  boolean update(@BindBean KonstantaO KonstantaO);

  /**
   * Menghapus Konstanta berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM konstanta WHERE id=:id")
  boolean delete(int id);
}
