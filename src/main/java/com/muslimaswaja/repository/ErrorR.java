package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel features.
 */
@RegisterRowMapper(ErrorO.Mapper.class)
public interface ErrorR {
 /**
   * Error berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT * FROM error WHERE id_jenis_kriminalitas=:idJenisKriminalitas ORDER BY id DESC LIMIT 1")
 ErrorO findByIdJenisKriminalitas(int idJenisKriminalitas);
 
 /**
   * Error berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT COUNT(*) FROM error WHERE id_jenis_kriminalitas=:idJenisKriminalitas")
 int check(int idJenisKriminalitas);
 
 /**
   * Menambahkan Error dan me-return id-nya.
   */
  @SqlUpdate("INSERT INTO error(id_jenis_kriminalitas, error) values(:idJenisKriminalitas, :error)")
  @GetGeneratedKeys
  int insert(@BindBean ErrorO ErrorO);
  
  
  /**
   * Meng-update Error berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE error SET id_jenis_kriminalitas=:idJenisKriminalitas, error=:error WHERE id=:id")
  boolean update(@BindBean ErrorO ErrorO);

  /**
   * Menghapus Error berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM error WHERE id=:id")
  boolean delete(int id);
}
