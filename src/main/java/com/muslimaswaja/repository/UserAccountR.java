package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel user_account.
 */
@RegisterRowMapper(UserAccountO.Mapper.class)
public interface UserAccountR {
  /**
   * Daftar User Account dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM user_account WHERE role=2 AND (nama ILIKE :keyword OR username ILIKE :keyword) ORDER BY id DESC LIMIT :max OFFSET :start")
  List<UserAccountO> list(int start, int max, String keyword);
  
  /**
   * Daftar User Account dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM user_account WHERE role=2 ORDER BY id DESC LIMIT 5")
  List<UserAccountO> listForDashboard();

  /**
   * User Account berdasarkan ID.
   */
 @SqlQuery("SELECT * FROM user_account WHERE id=:id")
 UserAccountO findById(int id);

  /**
   * User Account berdasarkan password.
   */
 @SqlQuery("SELECT * FROM user_account WHERE password=:password")
 UserAccountO findByPassword(String password);

  /**
   * Password berdasarkan username (untuk login Sudo).
   */
 @SqlQuery("SELECT password FROM user_account WHERE username=:username AND role=3")
 String findSudoPassword(String username);

  /**
   * Password berdasarkan username (untuk login Admin).
   */
 @SqlQuery("SELECT password FROM user_account WHERE username=:username AND role=2")
 String findAdminPassword(String username);

  /**
   * Total User Account administrator.
   */
 @SqlQuery("SELECT COUNT(*) FROM user_account WHERE role=2 AND (nama ILIKE :keyword OR username ILIKE :keyword)")
 int findTotal(String keyword);

  /**
   * Cek keberadaan username.
   */
 @SqlQuery("SELECT COUNT(*) FROM user_account WHERE username=:username")
 int isUsernameTaken(String username);

  /**
   * Cek keberadaan username dengan id yang berbeda.
   */
 @SqlQuery("SELECT COUNT(*) FROM user_account WHERE username=:username AND id!=:id")
 int isUsernameTaken(String username, int id);

  /**
   * Menambahkan User Account dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO user_account(username, password, nama, role) values(:username, :password, :nama, :role)")
  @GetGeneratedKeys
  int insert(@BindBean UserAccountO UserAccountO);

  /**
   * Meng-update User Account berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE user_account SET username=:username, password=:password, nama=:nama, role=:role WHERE id=:id")
  boolean update(@BindBean UserAccountO UserAccountO);

  /**
   * Meng-update Password User Account berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE user_account SET password=:password WHERE id=:id")
  boolean changePassword(String password, int id);

  /**
   * Menghapus User Account berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM user_account WHERE id=:id")
  boolean delete(int id);
}
