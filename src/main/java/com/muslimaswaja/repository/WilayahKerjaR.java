package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel konstanta.
 */
@RegisterRowMapper(WilayahKerjaO.Mapper.class)
public interface WilayahKerjaR {
  /**
   * Daftar semua Wilayah Kerja.
   */
  @SqlQuery("SELECT id, ST_AsText(geom) AS area, name FROM wilayah_kerja")
  List<WilayahKerjaO> list();
  
  /**
   * Hitung jumlah features yang termasuk.
   * SELECT id FROM gridsby WHERE ST_Contains(geom, ST_GeomFromText(:lokasi, 4326))
   */
  @SqlQuery("SELECT COUNT(*) FROM wilayah_kerja WHERE ST_Contains(geom, ST_GeomFromText(:lokasi, 4326))")
  int isInWilayahKerja(String lokasi);
}
