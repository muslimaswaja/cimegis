package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel laporan_kriminal.
 */
@RegisterRowMapper(LaporanKriminalO.Mapper.class)
@RegisterRowMapper(LaporanKriminalCsvO.Mapper.class)
@RegisterRowMapper(HeatmapO.Mapper.class)
public interface LaporanKriminalR {
  /**
   * Daftar data untuk heatmap tanpa filter.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal")
  List<HeatmapO> listAll();
  
  /**
   * Daftar data untuk heatmap dengan semua filter.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE id_jenis_kriminalitas=:idJenisKriminalitas AND EXTRACT(MONTH FROM waktu_kejadian)=:bulan AND waktu_kejadian::time BETWEEN to_timestamp(:waktuAwal, 'hh24:mi:ss')::time AND to_timestamp(:waktuAkhir, 'hh24:mi:ss')::time")
  List<HeatmapO> listAll(int idJenisKriminalitas, int bulan, String waktuAwal, String waktuAkhir);
  
  /**
   * Daftar data untuk heatmap dengan filter jenis kriminal.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE id_jenis_kriminalitas=:idJenisKriminalitas")
  List<HeatmapO> listAllByJenisKriminal(int idJenisKriminalitas);
  
  /**
   * Daftar data untuk heatmap dengan filter jenis kriminal dan bulan.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE id_jenis_kriminalitas=:idJenisKriminalitas AND EXTRACT(MONTH FROM waktu_kejadian)=:bulan")
  List<HeatmapO> listAllByJenisKriminal(int idJenisKriminalitas, int bulan);
  
  /**
   * Daftar data untuk heatmap dengan filter jenis kriminal dan waktu.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE id_jenis_kriminalitas=:idJenisKriminalitas AND waktu_kejadian::time BETWEEN to_timestamp(:waktuAwal, 'hh24:mi:ss')::time AND to_timestamp(:waktuAkhir, 'hh24:mi:ss')::time")
  List<HeatmapO> listAllByJenisKriminal(int idJenisKriminalitas, String waktuAwal, String waktuAkhir);
  
  /**
   * Daftar data untuk heatmap dengan filter bulan.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE EXTRACT(MONTH FROM waktu_kejadian)=:bulan")
  List<HeatmapO> listAllByBulan(int bulan);
  
  /**
   * Daftar data untuk heatmap dengan filter bulan dan waktu.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE EXTRACT(MONTH FROM waktu_kejadian)=:bulan AND waktu_kejadian::time BETWEEN to_timestamp(:waktuAwal, 'hh24:mi:ss')::time AND to_timestamp(:waktuAkhir, 'hh24:mi:ss')::time")
  List<HeatmapO> listAllByBulan(int bulan, String waktuAwal, String waktuAkhir);
  
  /**
   * Daftar data untuk heatmap dengan filter waktu.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE waktu_kejadian::time BETWEEN to_timestamp(:waktuAwal, 'hh24:mi:ss')::time AND to_timestamp(:waktuAkhir, 'hh24:mi:ss')::time")
  List<HeatmapO> listAllByWaktu(String waktuAwal, String waktuAkhir);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> list(int start, int max, String keyword);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis kriminalitas.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> list(int start, int max, int idJenisKriminalitas, String keyword);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis pelapor.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.role = :idRole AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> listByRole(int start, int max, int idRole, String keyword);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis pelapor dan jenis kriminalitas.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.role = :idRole AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> listByRole(int start, int max, int idRole, int idJenisKriminalitas, String keyword);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan id pelapor.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_pelapor = :idPelapor AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> listByPelapor(int start, int max, int idPelapor, String keyword);
  
  /**
   * Daftar Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan id pelapor dan jenis kriminalitas.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_pelapor = :idPelapor AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<LaporanKriminalO> listByPelapor(int start, int max, int idPelapor, int idJenisKriminalitas, String keyword);
  
  /**
   * Daftar Laporan Kriminal untuk dashboard halaman admin.
   */
  @SqlQuery("SELECT lk.id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id ORDER BY id DESC LIMIT 5 OFFSET 0")
  List<LaporanKriminalO> listForDashboard();
  
  /**
   * Daftar Laporan Kriminal untuk file CSV.
   */
  @SqlQuery("SELECT ua.nama AS pelapor, jk.jenis, lk.waktu_kejadian, ST_AsText(lk.lokasi) AS koordinat FROM user_account AS ua, laporan_kriminal AS lk, jenis_kriminalitas AS jk WHERE lk.id_pelapor=ua.id AND lk.id_jenis_kriminalitas=jk.id ORDER BY lk.id DESC")
  List<LaporanKriminalCsvO> listForCSV();
  
  /**
   * Daftar koordinat untuk penghitungan metode.
   */
  @SqlQuery("SELECT ST_AsText(lokasi) AS koordinat FROM laporan_kriminal WHERE id_jenis_kriminalitas=:idJenisKriminalitas ORDER BY id DESC")
  List<String> listForMethod(int idJenisKriminalitas);

  /**
   * Laporan Kriminal berdasarkan ID.
   */
 @SqlQuery("SELECT id, id_jenis_kriminalitas, id_pelapor, waktu_kejadian, ST_AsText(lokasi) as lokasi FROM laporan_kriminal WHERE id=:id")
 LaporanKriminalO findById(int id);
 
 /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.nama ILIKE :keyword")
  int total(String keyword);
  
  /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis kriminalitas.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword")
  int total(int idJenisKriminalitas, String keyword);
  
  /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis pelapor.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.role = :idRole AND ua.nama ILIKE :keyword")
  int totalByRole(int idRole, String keyword);
  
  /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan jenis pelapor dan jenis kriminalitas.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND ua.role = :idRole AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword")
  int totalByRole(int idRole, int idJenisKriminalitas, String keyword);
  
  /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan id pelapor.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_pelapor = :idPelapor AND ua.nama ILIKE :keyword")
  int totalByPelapor(int idPelapor, String keyword);
  
  /**
   * Total Laporan Kriminal dengan jumlah maksimal max data dan dimulai dari start dan disaring dengan id pelapor dan jenis kriminalitas.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal AS lk, user_account AS ua WHERE lk.id_pelapor = ua.id AND lk.id_pelapor = :idPelapor AND lk.id_jenis_kriminalitas = :idJenisKriminalitas AND ua.nama ILIKE :keyword")
  int totalByPelapor(int idPelapor, int idJenisKriminalitas, String keyword);
  
  /**
   * Total jumlah laporan kriminal tertentu dalam radius 1 kilometer.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal WHERE id_jenis_kriminalitas=:jenis AND ST_Distance(ST_transform(lokasi, 26986), ST_Transform(ST_GeomFromText(:koordinat, 4326), 26986))\\<1000")
  int jumlahLaporanRadius(int jenis, String koordinat);
  
  /**
   * Total jumlah laporan kriminal tertentu dalam radius 1 kilometer dengan filter bulan.
   */
  @SqlQuery("SELECT COUNT(*) FROM laporan_kriminal WHERE id_jenis_kriminalitas=:jenis AND EXTRACT(MONTH FROM waktu_kejadian)=:bulan AND ST_Distance(ST_transform(lokasi, 26986), ST_Transform(ST_GeomFromText(:koordinat, 4326), 26986))\\<1000")
  int jumlahLaporanRadius(int jenis, int bulan, String koordinat);

  /**
   * Menambahkan Laporan Kriminal dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO laporan_kriminal(id_jenis_kriminalitas, id_pelapor, lokasi, waktu_kejadian) values(:idJenisKriminalitas, :idPelapor, ST_GeomFromText(:lokasi,4326), to_timestamp(:waktuKejadian, 'YYYY-MM-DD HH24:MI:MS'))")
  @GetGeneratedKeys
  int insert(@BindBean LaporanKriminalO LaporanKriminalO);

  /**
   * Meng-update Laporan Kriminal berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE laporan_kriminal SET id_jenis_kriminalitas=:idJenisKriminalitas, id_pelapor=:idPelapor, lokasi=ST_GeomFromText(:lokasi,4326), waktu_kejadian=to_timestamp(:waktuKejadian, 'YYYY-MM-DD HH24:MI:MS') WHERE id=:id")
  boolean update(@BindBean LaporanKriminalO LaporanKriminalO);

  /**
   * Menghapus Laporan Kriminal berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM laporan_kriminal WHERE id=:id")
  boolean delete(int id);
}
