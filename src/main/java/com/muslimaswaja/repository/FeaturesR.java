package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel features.
 */
@RegisterRowMapper(FeaturesO.Mapper.class)
@RegisterRowMapper(FeaturesTerdekatO.Mapper.class)
public interface FeaturesR {
  /**
   * Daftar Features dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT id, id_jenis_lokasi, nama, ST_AsText(lokasi), waktu_input, keterangan FROM features WHERE nama ILIKE :keyword OR keterangan ILIKE :keyword ORDER BY id DESC LIMIT :max OFFSET :start")
  List<FeaturesO> list(int start, int max, String keyword);
  
  /**
   * Daftar Features dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT id, id_jenis_lokasi, nama, ST_AsText(lokasi), waktu_input, keterangan FROM features WHERE id_jenis_lokasi=:idJenisLokasi AND (nama ILIKE :keyword OR keterangan ILIKE :keyword) ORDER BY id DESC LIMIT :max OFFSET :start")
  List<FeaturesO> list(int start, int max, int idJenisLokasi, String keyword);

  /**
   * Daftar Features dengan untuk dashboard.
   */
  @SqlQuery("SELECT id, id_jenis_lokasi, nama, ST_AsText(lokasi), waktu_input, keterangan FROM features ORDER BY id DESC LIMIT 5")
  List<FeaturesO> listForDashboard();
  
  /**
   * Features berdasarkan ID.
   */
 @SqlQuery("SELECT id, id_jenis_lokasi, nama, ST_AsText(lokasi), waktu_input, keterangan FROM features WHERE id=:id")
 FeaturesO findById(int id);
  
  /**
   * Features terdekat berdasarkan jenis lokasi.
   */
 @SqlQuery("SELECT f.nama, jl.jenis AS jenis_lokasi, ST_AsText(f.lokasi) AS lokasi, ST_Distance(ST_Transform(f.lokasi, 26986), ST_Transform(ST_GeomFromText(:lokasi, 4326), 26986)) as jarak FROM features AS f, jenis_lokasi AS jl WHERE f.id_jenis_lokasi=jl.id AND id_jenis_lokasi=:idJenisLokasi ORDER BY jarak LIMIT 1")
 FeaturesTerdekatO findTerdekat(int idJenisLokasi, String lokasi);
  
  /**
   * Jarak berdasarkan id jenis lokasi dan koordinat.
   */
 @SqlQuery("SELECT ST_Distance(ST_Transform(lokasi, 26986), ST_Transform(ST_GeomFromText(:koordinat, 4326), 26986)) as jarak FROM features WHERE id_jenis_lokasi=:idJenisLokasi ORDER BY jarak LIMIT 1")
 double findJarak(int idJenisLokasi, String koordinat);
 
 /**
   * Total Features.
   */
 @SqlQuery("SELECT COUNT(*) FROM features WHERE nama ILIKE :keyword OR keterangan ILIKE :keyword")
 int findTotal(String keyword);
 
 /**
   * Total Features berdasarkan kategori.
   */
 @SqlQuery("SELECT COUNT(*) FROM features WHERE id_jenis_lokasi=:idJenisLokasi AND (nama ILIKE :keyword OR keterangan ILIKE :keyword)")
 int findTotal(int idJenisLokasi, String keyword);

  /**
   * Menambahkan Features dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO features(id_jenis_lokasi, nama, lokasi, waktu_input, keterangan) values(:idJenisLokasi, :nama, ST_GeomFromText(:lokasi,4326), to_timestamp(:waktuInput, 'YYYY-MM-DD HH24:MI:MS'), :keterangan)")
  @GetGeneratedKeys
  int insert(@BindBean FeaturesO FeaturesO);

  /**
   * Meng-update Jenis Lokasi berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE features SET id_jenis_lokasi=:idJenisLokasi, nama=:nama, lokasi=ST_GeomFromText(:lokasi,4326), keterangan=:keterangan WHERE id=:id")
  boolean update(@BindBean FeaturesO FeaturesO);

  /**
   * Menghapus Features berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM features WHERE id=:id")
  boolean delete(int id);
}
