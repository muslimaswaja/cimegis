package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel jenis_kriminalitas.
 */
@RegisterRowMapper(JenisKriminalitasO.Mapper.class)
public interface JenisKriminalitasR {
  /**
   * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM jenis_kriminalitas")
  List<JenisKriminalitasO> listAll();
  
  /**
   * Daftar Jenis Lokasi dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM jenis_kriminalitas limit :max offset :start")
  List<JenisKriminalitasO> list(int start, int max);

  /**
   * Jenis Lokasi berdasarkan ID.
   */
 @SqlQuery("SELECT * FROM jenis_kriminalitas WHERE id=:id")
 JenisKriminalitasO findById(int id);

  /**
   * Menambahkan Jenis Lokasi dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO jenis_kriminalitas(jenis) values(:jenis)")
  @GetGeneratedKeys
  int insert(@BindBean JenisKriminalitasO JenisKriminalitasO);

  /**
   * Meng-update Jenis Lokasi berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE jenis_kriminalitas SET jenis=:jenis WHERE id=:id")
  boolean update(@BindBean JenisKriminalitasO JenisKriminalitasO);

  /**
   * Menghapus Jenis Lokasi berdasar ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM jenis_kriminalitas WHERE id=:id")
  boolean delete(int id);
}
