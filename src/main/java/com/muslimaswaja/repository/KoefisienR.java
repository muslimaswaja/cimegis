package com.muslimaswaja.repository;

import com.muslimaswaja.object.*;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel koefisien.
 */
@RegisterRowMapper(KoefisienO.Mapper.class)
public interface KoefisienR {
  /**
   * Daftar Koefisien dengan jumlah maksimal max data dan dimulai dari start.
   */
  @SqlQuery("SELECT * FROM koefisien limit :max offset :start")
  List<KoefisienO> list(int start, int max);

  /**
   * Koefisien berdasarkan ID.
   */
 @SqlQuery("SELECT * FROM koefisien WHERE id=:id")
 KoefisienO findById(int id);

  /**
   * Koefisien berdasarkan id jenis kriminalitas.
   */
 @SqlQuery("SELECT * FROM koefisien WHERE id_jenis_kriminalitas=:idJenisKriminalitas")
 List<KoefisienO> findByIdJenisKriminalitas(int idJenisKriminalitas);
 
  /**
   * Cek keberadaan koefisien berdasarkan id jenis kriminalitas dan id jenis lokasi.
   */
 @SqlQuery("SELECT id FROM koefisien WHERE id_jenis_kriminalitas=:idJenisKriminalitas AND id_jenis_lokasi=:idJenisLokasi")
 int findId(int idJenisKriminalitas, int idJenisLokasi);
 
  /**
   * Nilai koefisien berdasarkan id jenis kriminalitas dan id jenis lokasi.
   */
 @SqlQuery("SELECT * FROM koefisien WHERE id_jenis_kriminalitas=:idJenisKriminalitas id_jenis_lokasi=:idJenisLokasi ORDER BY id DESC LIMIT 1")
 KoefisienO findKoefisien(int idJenisKriminalitas, int idJenisLokasi);
 
  /**
   * Cek keberadaan koefisien berdasarkan id jenis kriminalitas dan id jenis lokasi.
   */
 @SqlQuery("SELECT COUNT(*) FROM koefisien WHERE id_jenis_kriminalitas=:idJenisKriminalitas AND id_jenis_lokasi=:idJenisLokasi")
 int isExist(int idJenisKriminalitas, int idJenisLokasi);

  /**
   * Menambahkan Koefisien dan me-return keberhasilannya.
   */
  @SqlUpdate("INSERT INTO koefisien(id_jenis_kriminalitas, id_jenis_lokasi, nilai, waktu_input, korelasi) values(:idJenisKriminalitas, :idJenisLokasi, :nilai, to_timestamp(:waktuInput, 'YYYY-MM-DD HH24:MI:MS'), :korelasi)")
  @GetGeneratedKeys
  int insert(@BindBean KoefisienO KoefisienO);

  /**
   * Meng-update Koefisien berdasarkan ID-nya dan me-return keberhasilannya.
   */
  @SqlUpdate("UPDATE koefisien SET id_jenis_kriminalitas=:idJenisKriminalitas, id_jenis_lokasi=:idJenisLokasi, nilai=:nilai, korelasi=:korelasi WHERE id=:id")
  boolean update(@BindBean KoefisienO KoefisienO);

  /**
   * Menghapus Koefisien berdasarkan id jenis kriminalitas dan me-return keberhasilannya.
   */
  @SqlUpdate("DELETE FROM koefisien WHERE id_jenis_kriminalitas=:idJenisKriminalitas")
  boolean delete(int idJenisKriminalitas);
}
