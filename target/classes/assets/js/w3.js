function showBlock(id) {
	document.getElementById(id).style.display = "block";
}

function show(id) {
	document.getElementById(id).style.display = null;
}

function hide(id) {
	document.getElementById(id).style.display = "none";
}

function hideNotif(element) {
	element.parentNode.parentNode.style.display = "none";
}
